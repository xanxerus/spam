#!/usr/bin/env pypy

import json
from sys import argv
from time import time

from xutils import parse_pa
from xutils.xfield import GaloisField

def uniquely_covered_position(coset, P, Q, p):
	ret = 0
	for pi in coset:
		if pi[p] in Q:					#if the perm is covered at p
			count = 0
			for pot in P:				#check if the perm is covered elsewhere
				if pi[pot] in Q:		#and mark it as so
					count += 1
			if count == 0:				#if it isn't covered elsewhere
				ret += 1				#increment our count
	
	return ret

def uniquely_covered_symbol(coset, P, Q, q):
	ret = 0
	for pi in coset:
		qSeen = False
		for p in P:
			if pi[p] == q:
				qSeen = True
			elif pi[p] in Q:
				break
		else:
			if qSeen:
				ret += 1
	
	return ret

def cov_deg_p(coset, Q, p):
	ret = 0
	for pi in coset:
		if pi[p] in Q:
			ret += 1
	return ret

def cov_deg_q(coset, P, q):
	ret = 0
	for pi in coset:
		for p in P:
			if pi[p] == Q:
				ret += 1
	return ret

def whoop(arr, a, b):
	ret = [b]
	for x in arr:
		if x != a:
			ret.append(x)
	return ret

def p_exchange(P, ka, pa, kb, pb):
	ret = []
	for k, P_k in enumerate(P):
		if k == ka:
			ret.append(whoop(P_k, pa, pb))
		elif k == kb:
			ret.append(whoop(P_k, pb, pa))
		else:
			ret.append(P_k)
	return ret

def boop(pi, p):
	ret = pi[:]
	ret.append(ret[p])
	ret[p] = len(ret) - 1

def rawr(perm, s, k, P1, Q1, P2, Q2):
	S, K = len(P2), len(P1[0])
	N = len(perm)
	if k < K and s < S: #collision potential

		# first level covered positions
		fl = set()
		for p in P1[s][k]:
			if perm[p] in Q1[s][k]:
				fl.add(p)

		# second level covered positions			
		sl = set()
		for p in P2[s]:
			if p != N and perm[p] in Q2[s]:
				sl.add(p)

		# here be dragons
		if len(fl - sl) and sl: #fl is not a subset of sl
			p = (fl-sl).pop()
			perm.append(perm[p])
			perm[p] = N
			
			sl.discard(p)

			p = sl.pop()
			perm.append(perm[p])
			perm[p] = N+1
			return perm
		elif len(sl) > 1 and fl: # |sl| > 1
			p = fl.pop()
			perm.append(perm[p])
			perm[p] = N
			
			sl.discard(p)

			p = sl.pop()
			perm.append(perm[p])
			perm[p] = N+1
			return perm
		else: #clash is inevitable unless we can collude
			#Lv1 can put a symbol in Q2 into a position in N
			if N in P2[s]: 
				for p in fl:
					if perm[p] in Q2[s]: #collusion type 1, feed q to N
						perm.append(N+1)
						perm.append(perm[p])
						perm[p] = N
						return perm
						break
			
			#Lv1 can put N into a position in P2
			if N in Q2[s]: 
				for p in fl:
					if p in P2[s]: #collusion type 2, feed N to p
						perm.append(perm[p])
						perm.append(N)
						perm[p] = N+1
						return perm
						break

	else: #no clash potential, something is a freebie
		fl = False
		#first level
		if k < K:
			for p in P1[s][k]:
				if perm[p] in Q1[s][k]:
					perm.append(perm[p])
					perm[p] = N
					fl = True
					break
		elif k == K:
			fl = True
			perm = perm + [N]
		else:
			raise ValueError('Error -42: Xander sucks. Contact him.')

		if not fl:
			return None
		else:
			fl = False

		#second level
		if s < S:
			for p in P2[s]:
				#~ print len(perm), p, len(Q2), s
				if perm[p] in Q2[s]:
					perm.append(perm[p])
					perm[p] = N+1
					fl = True
					break
		elif s == S:
			fl = True
			perm = perm + [N+1]
		else:
			raise ValueError('Error -43: Xander sucks. Contact him.')

		if fl:
			return perm

def har(perm, s, k, P1, Q1, P2, Q2):
	S, K = len(P2), len(P1[0])
	N = len(perm)
	if k < K and s < S: #collision potential

		# first level covered positions
		fl = set()
		for p in P1[s][k]:
			if perm[p] in Q1[s][k]:
				fl.add(p)

		# second level covered positions			
		sl = set()
		for p in P2[s]:
			if p != N and perm[p] in Q2[s]:
				sl.add(p)

		# here be dragons
		if len(fl - sl) and sl: #fl is not a subset of sl
			p = (fl-sl).pop()
			perm.append(perm[p])
			perm[p] = N
			
			sl.discard(p)

			p = sl.pop()
			perm.append(perm[p])
			perm[p] = N+1
			return 'A'
		elif len(sl) > 1 and fl: # |sl| > 1
			p = fl.pop()
			perm.append(perm[p])
			perm[p] = N
			
			sl.discard(p)

			p = sl.pop()
			perm.append(perm[p])
			perm[p] = N+1
			return 'B'
		else: #clash is inevitable unless we can collude
			#Lv1 can put a symbol in Q2 into a position in N
			if N in P2[s]: 
				for p in fl:
					if perm[p] in Q2[s]: #collusion type 1, feed q to N
						perm.append(N+1)
						perm.append(perm[p])
						perm[p] = N
						qwer.append(perm)
						break
			
			#Lv1 can put N into a position in P2
			if N in Q2[s]: 
				for p in fl:
					if p in P2[s]: #collusion type 2, feed N to p
						perm.append(perm[p])
						perm.append(N)
						perm[p] = N+1
						return 'C'
						break

	else: #no clash potential, something is a freebie
		fl = False
		#first level
		if k < K:
			for p in P1[s][k]:
				if perm[p] in Q1[s][k]:
					perm.append(perm[p])
					perm[p] = N
					fl = True
					break
		elif k == K:
			fl = True
			perm = perm + [N]
		else:
			raise ValueError('Error -42: Xander sucks. Contact him.')

		if not fl:
			return False
		else:
			fl = False

		#second level
		if s < S:
			for p in P2[s]:
				#~ print len(perm), p, len(Q2), s
				if perm[p] in Q2[s]:
					perm.append(perm[p])
					perm[p] = N+1
					fl = True
					return 'E'
					break
		elif s == S:
			fl = True
			perm = perm + [N+1]
			return 'F'
		else:
			raise ValueError('Error -43: Xander sucks. Contact him.')

def bleh(perm, s, k, P1, Q1, P2, Q2):
	S, K, N = len(P2), len(P1[0]), len(perm)
	if s == S and k == K: # double freebie
		return True

	elif s == S: # level 2 freebie
		for p in P1[s][k]:
			if perm[p] in Q1[s][k]:
				return True
	
	elif k == K: # level 1 freebie
		for p2 in P2[s]:
			if p2 == len(perm):
				if len(perm) in Q2[s]:
					return True
			else:
				if perm[p2] in Q2[s]:
					return True		
	
	else: #collision potential

		# first level covered positions
		fl = set()
		for p in P1[s][k]:
			if perm[p] in Q1[s][k]:
				fl.add(p)

		# second level covered positions			
		sl = set()
		for p in P2[s]:
			if p != N and perm[p] in Q2[s]:
				sl.add(p)
		
		# here be dragons
		if len(fl - sl) and sl: #fl is not a subset of sl
			return True
		if len(sl) > 1 and fl: # |sl| > 1
			return True
		else: #clash is inevitable unless we can collude
			#Lv1 can put a symbol in Q2 into a position in N
			if N in P2[s]: 
				for p in fl:
					if perm[p] in Q2[s]: #collusion type 1, feed q to N
						# print 'collusion 1'
						return True
			
			#Lv1 can put N into a position in P2
			if N in Q2[s]: 
				for p in fl:
					if p in P2[s]: #collusion type 2, feed N to p
						# print 'collusion 2'
						return True

	return False

def collusion(pa, P1, Q1, P2, Q2, generate=False):
	S, K = len(P2), len(P1[0])
	N = len(pa[0][0])
	Q1 = [map(set, Q) for Q in Q1]
	Q2 = map(set, Q2)

	ret = [] if generate else 0
	for a, coset in enumerate(pa):
		s, k = divmod(a, K+1)
		for perm in coset:
			if generate:
				a = rawr(perm[:], s, k, P1, Q1, P2, Q2)
				if a:
					ret.append(a)
					
			else:
				if bleh(perm, s, k, P1, Q1, P2, Q2):
					ret += 1

	return ret

def pi_count(pa, s, P1, Q1, P2, Q2):
	K = len(P1[0])
	ret = 0
	for a in xrange(s*(K+1), (s+1)*(K+1)):
		for perm in pa[a]:
			if bleh(perm[:], s, a % (K+1), P1, Q1, P2, Q2):
				ret += 1
	return ret

def main(filename):

	with open(filename, 'r') as infile:
		data = json.load(infile)

	P1 = data['P1']
	Q1 = data['Q1']
	P2 = data['P2']
	Q2 = data['Q2']

	numCosets = sum(map(len, P1)) + len(P1)

	print len(P1), len(Q1), len(P2), len(Q2), map(len, P1), map(len, Q1)
	p, r = data['pa']['n'], data['pa']['r']
	q = p**r
	f = GaloisField(p, r, prim=data['pa']['prim'])

	pa = []
	for a in xrange(1, min(f.Q, numCosets+1)):
		coset = []
		for b in f.elements:
			coset.append([int(f(a)*x + b) for x in f.elements])
		pa.append(coset)

	print collusion(pa, P1, Q1, P2, Q2)

	S, K = len(P2), len(P1[0])

	iteration = 0
	while True:
		start_t = time()
		iteration += 1
		print 'Iteration %d' % iteration
		s_lut = [pi_count(pa, s, P1, Q1, P2, Q2) for s in xrange(S+1)]
		sums_lut = sum(s_lut)

		bestdiff = 0
		bestP2 = None

		for sa in xrange(S):
			for sb in xrange(S):
				if sa == sb:
					continue

				cov_old = s_lut[sa] + s_lut[sb]
				for px in P2[sa]:
					for py in P2[sb]:
						P_pot = p_exchange(P2, sa, px, sb, py)
						diff = pi_count(pa, sa, P1, Q1, P_pot, Q2) + pi_count(pa, sb, P1, Q1, P_pot, Q2) - cov_old
						if diff > bestdiff:
							bestdiff = diff
							bestP2 = P_pot
							print 'New best coverage: ', bestdiff+sums_lut
							print P2

							with open("%d" % (bestdiff+sums_lut), 'w+') as outfile:
								for perm in collusion(pa, P1, Q1, P_pot, Q2, generate=True):
									outfile.write(" ".join(map(str, perm)) + '\n')
							print 'written'

		P2 = bestP2
		print time() - start_t

HELPSTR = '''
dev_brute.py will try every swapping of positions between cosets
to find improvements in coverage. It does not try symbols yet.
Also it seems like it counts wrong.

Usage:
	pypy dev_brute.py INFILE

Where:
	INFILE is an xtar file of type "sequential" or "pi"


'''.strip()

if __name__ == '__main__':
	from sys import argv
	if len(argv) > 1:
		main(argv[1])
	else:
		print HELPSTR
