#!/usr/bin/env pypy

def uniquely_covered_position(coset, P, Q, p):
	ret = 0
	for pi in coset:
		if pi[p] in Q:					#if the perm is covered at p
			count = 0
			for pot in P:				#check if the perm is covered elsewhere
				if pi[pot] in Q:		#and mark it as so
					count += 1
			if count == 0:				#if it isn't covered elsewhere
				ret += 1				#increment our count
	
	return ret

def PfromQ(pa, Q):
	N = len(pa[0][0])
	P = [[] for _ in Q]
	for p in xrange(N):
		bestK = 0
		bestCov = 0
		for k, coset in enumerate(pa):
			potCov = uniquely_covered_position(coset, P[k], Q[k], p)
			if potCov > bestCov:
				bestK = k
				bestCov = potCov
		P[bestK].append(p)
	return P

def split_Q(N, K):
	ret = []
	d, m = divmod(N, K)
	i = 0
	for x in xrange(K):
		if x < m:
			ret.append(range(i, i+d+1))
			i += d+1
		else:
			ret.append(range(i,i+d))
			i += d
	return ret

def calc_cov(f, a, K, remainingP, Q2_s):
	c_lut = {p:set() for p in remainingP}
	# calculate coverage
	for g_a in f.elements[a:a + 1 + K]:
		for p in remainingP:
			u = g_a * f(p)
			for b in f.elements:
				if int(u + b) in Q2_s:
					c_lut[p].add((a,b))
		a += 1

	return c_lut

def select_p(S, c_lut, remainingP):
	ret = []
	# get best minus overlap
	for _ in xrange(S):

		# pick the best
		bestP = None
		for p, cov in c_lut.iteritems():
			if bestP is None or len(cov) > len(c_lut[bestP]):
				bestP = p

		if bestP is None:
			break

		# add it
		ret.append(bestP)
		remainingP.discard(bestP)

		# subtract overlaps
		bestSet = c_lut[bestP]
		del c_lut[bestP]
		for p in c_lut:
			c_lut[p] -= bestSet

	return ret

def main(filename):
	from datetime import datetime
	
	# parse args
	with open(filename, 'r') as infile:
		data = json.load(infile)

	print 'Parsing'
	P1 = data['P1']
	Q1 = data['Q1']
	K = len(P1[0])

	p = data['pa']['n']
	r = data['pa']['r']
	q = p**r
	f = GaloisField(p, r, prim=data['pa']['prim'])

	numCosets = sum(map(len, P1)) + len(P1)
	Q2 = split_Q(q+1, len(P1)-1)

	# calculate a covering of each pi
	remainingP = set(xrange(q))
	P2 = []
	c_lut_lut = []
	for s in xrange(len(P1)-1):
		print 'Processing pi %d of %d' % (s+1, len(P1)-1), datetime.now()
		c_lut_lut.append(calc_cov(f, 1 + s*(K+1), K, remainingP, set(Q2[s])))
		P2.append(select_p(len(Q2[s]), c_lut_lut[s], remainingP))

	data['filetype'] = 'sequential'
	data['P2'] = P2
	data['Q2'] = Q2

	with open('%d_%d.xtar' % (q+2, q), 'w+') as outfile:
		json.dump(data, outfile)


HELPSTR = '''
dev_naive_cover.py will create a covering of an xtar file of type "pi".
It will do its best to cover this file, which will be output as Q+2_Q.xtar,
where Q is the prime power of AGL.

Usage:
	pypy dev_naive_cover.py INFILE

Where:
	INFILE is an xtar file of type "sequential" or "pi"


'''.strip()

if __name__ == '__main__':
	# import things
	from dev_brute import collusion, bleh
	from xutils import parse_pa
	from xutils.xfield import GaloisField

	from collections import Counter
	import json

	from sys import argv

	import cProfile

	if len(argv) > 1:
		# cProfile.run('main(argv[1])')
		main(argv[1])
	else:
		print HELPSTR
