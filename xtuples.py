
HELPSTR = '''
xtuples.py generates the tuples for the elements of a Galois Field

Usage:
	python xtuples.py Q [options]

Where:
	Q is either a prime, a prime power, or a string of the form:
		P^R, where P is prime and R is a prime power


Options:
	-r, --primitive, specify the coefficients of the field's primitive polynomial in ascending order of degree
				as a space separated list of integers

'''.strip()


from xutils.xfield import GaloisField
from xutils import parsePR, gamma, gammainverse
from sys import argv

if __name__ == '__main__':
    if len(argv) < 2:
        print HELPSTR
        exit(1)

    N, R = parsePR(argv[1])
    NR = N**R

    try:
        PP = map(int, argv[2:]) or None
    except:
        print HELPSTR
        exit(1)

    F = GaloisField(N, R, prim=PP)
    print 'Generating GF(%s) with PP: %s' % (F.Q, F.prim.coef)
    for x in range(NR):
        print x, F(x).coef
