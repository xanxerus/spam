#!/usr/bin/pypy -i

HELPSTR = '''
xpp.py generates permutations by doing reed solomon with rings

Usage:
	python xpp.py N D

Where:
	N is a natural number
	D is the desired hamming distance

Options:
	-v, --verbose, i.e, prints message vectors and codeword vectors

'''.strip()

from xutils.xring import Ring
from xutils import pp
from time import time
import xtar

def increment(t, mod, fixations=None):
    if fixations is None:
        fixations = tuple()

    for i in reversed(xrange(len(t))):
        if i in fixations:
            if isinstance(fixations[i], int):
                floor, ceil = fixations[i], fixations[i]
            else:
                floor, ceil = fixations[i][0], fixations[i][1]
        else:
            floor, ceil = 0, mod

        if t[i] + 1 >= ceil:
            t[i] = floor
        else:
            t[i] += 1
            return t
    else:
        return None

# Q is modulus, M is message length
def allMessages(Q, M, fixations=None, start=None):
    t = start or [0] * M
    if fixations:
        for p in fixations:
            fix = fixations[p] if isinstance(fixations[p], int) else fixations[p][0]
            if p < M:
                t[p] = fix
            else:
                raise StopIteration

    while t is not None:
        yield t and t[:]
        t = increment(t, Q, fixations)


# group arithmetic, with trick, horner method, and restrictions
def gthr(N, D, verbose=False):
    # permutationPolynomials = []
    f = Ring(N)

    filename = 'PP_%d_%d.txt' % (N, D)
    sofar = 0

    try:
        # pps = []
        pa = []
        last_time = time()
        # START all messages for loop
        for msg_idx, msg in enumerate(allMessages(N, D)):
            if verbose:
                print 'Message %d: %s' % (msg_idx, msg + [0])
            if time() - last_time > 60:
                with open(filename, 'a') as buf:
                    # for p in pps:
                    for p in pa:
                        # buf.write(pp(p + [0]) + '\n')
                        buf.write(pp(p) + '\n')

                # pps = []
                pa = []
                last_time = time()

            # here we calculate the codeword's elements.
            elementLut = [False] * N
            codeword = []

            # the t'th element of the codeword will be calculated on iteration t
            for t in xrange(N):
                cell = f(0)
                for i in xrange(D):
                    # print i, cell, f(msg[i]), f(t)
                    cell = (cell + f(msg[i])) * f(t)

                if elementLut[int(cell)]:
                    break
                else:
                    elementLut[int(cell)] = True
                    codeword.append(cell)
            else:
                # pps.append(msg)
                pa.append(codeword)
                s = pp(msg + [0])
                sofar += 1
                print '%d: %s' % (sofar, s)
                # print '%d: %s' % (sofar, pp(codeword))

        # END all messages for loop
        print 'Calculations complete'

        with open(filename, 'a') as buf:
            # for p in pps:
            for p in pa:
                # buf.write(pp(p + [0]) + '\n')
                buf.write(pp(p) + '\n')

    except KeyboardInterrupt:
        print 'Calculations halted'

    print 'In total, %d polynomials were found.' % sofar
    return True


from sys import argv

if __name__ == '__main__':
    if len(argv) != 3:
        print HELPSTR
        exit(1)

    N, D = int(argv[1]), int(argv[2])
    gthr(N, D)