#!/usr/bin/env pypy

HELP = '''
xprf.py generates the permutations formed by permutation rational functions.

Given a primitive polynomial and a file full of permutations representing
PRFs in their normal form P(x) / Q(x), this program will print out all
permutations of the form P(x+a) / Q(x+a) + b for all a, b

Usage:
  pypy xprf.py FILENAME P R X0 X1 X2 X3

Where:
  FILENAME is the name of a file full of permutations
  P is the prime base of the Galois field
  R is the exponent of the prime of the Galois field
  X0 X1 X2 X3... are the coefficients of the primitive polynomial,
    such that Xi is the coefficient of x^i

(Example:) For GF(3^4) with primitive polynomial 2 + x^3 + x^4, use:
  pypy xprf.py ~/tmp/test.txt 3 4 2 0 0 1 1


'''.strip()

from xutils.xfield import GaloisField
from sys import argv

def prfify(f, perm):
  fperm = map(f, perm)
  for b in f.elements:
    for a in f.elements:
      #yield [fperm[int(j+b)]+a for j in f.elements]
      yield [int(fperm[int(j+b)]+a) for j in f.elements]


if __name__ == '__main__':
  try:
    filename = argv[1]
    n, r = int(argv[2]), int(argv[3])
    prim = map(int, argv[4:])
  except:
    print HELP

  f = GaloisField(n, r, prim)

  with open(filename, 'r') as infile:
    for line in infile:
      perm = map(int, line.strip().split())
      for o in prfify(f, perm):
        print ' '.join(map(str, o))
