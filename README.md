SPAM - Sudborough's Permutation Array Maker

SPAM is an effort to put all of Xander's research code into one package
for three main reasons:

1. To give all the programs I made a common utilities package, xutils
2. To make it easier for all the programs to use the new compression method xtar
3. To make it easier to use these programs with Tommy's server

Every script has these properties:
- usage can be found by running the script with the --help option
- written in Python 2.7
- will run faster using pypy, but runs fine with regular python
- Does file output in xtared format unless the -x format is specified

Here are the highlights:
- xtar      - compress or decompress permutation arrays into compact formats
- xcontract - perform product block construction
- xgroup    - construct a group
- xoddsud   - apply parity argument to odd powered AGL. PE or pis.
- xrs       - various functions relating to reed solomon codes

For an explanation of the xtar compression method, go to this URL:
https://docs.google.com/document/d/1OxJBE7PdyqeHSXYtVV16geWCn-uvJxRm5gncCaU2QKI/edit?usp=sharing
