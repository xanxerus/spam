from xverify import allperms
from xtar import extract
import json

HELPSTR = '''
Usage: 
    pypy xallperms.py FILENAME N

Where:
    FILENAME is a PA file or a .xtar file
    N is the number of symbols in a permutation

'''.strip()

if __name__ == '__main__':
	from sys import argv

	def FilePa(filename):
		with open(filename, 'r') as f:
			for line in f:
				perm = line.strip().split()
				if perm:
					yield map(int, perm)

	try:
		filename = argv[1]
		N = int(argv[2])
	except:
		print HELPSTR
		exit(1)
		
	if filename.endswith('xtar'):
		data = json.load(open(filename, 'r'))
		gen = (perm for perm in extract(mode='generator', **data) if perm)
	else:	
		gen = FilePa(filename)

	x = allperms(gen, N)
	if x is None:
		print 'All permutations'
	else:
		print 'Non-permutation at line', x
