#!/usr/bin/pypy -i

HELPSTR = '''
xrs.py generates the permutation elements of a reed solomon code.

Usage:
	python xrs.py N D p1 q1 p2 q2 ... pn qn [options]

Where:
	N is a prime or prime power
	D is the desired hamming distance (or polynomial degree if in Permutation Polynomial mode)
	p1...pn and q1...qn are coefficient restrictions,
		For example, the command "$python xrs.py 2 5 1 0 3 1:4" 
		would fix the coefficient of the x^(D-1) term at 0
		and the coefficient of the x^(D-3) term within the interval [1, 4)
		excluding 4.

	Note: coefficient restrictions are only allowed in naive mode

Options:
	-n, --naive, naive mode, i.e., without divide and conquer
	-v, --verbose, i.e, prints message vectors and codeword vectors
	-p, --polynomial, Permutation Polynomial mode, see below
	-r, --primitive, specify the coefficients of the field's primitive polynomial in ascending order of degree
				as a space separated list of integers


Permutation Polynomial Mode
	In Permutation Polynomial mode, we will write the messages instead of codewords
	to a file. We will interpret D as the degree of the polynomial instead of a hamming distance

	python xrs.py N D p1 q1 p2 q2 ... pn qn -p


Default behavior is to use Bereg's algorithm with divide and conquer


ALTERNATIVE USAGE:
	python xrs.py FILENAME

	If the given file contains an xrs.py backup, that calculation will
	be resumed.

'''.strip()


from xutils.xfield import GaloisField, Polynomial, setModulus, zeropad
from xutils import pp, parsePR, timestamp
from threading import Thread
import xtar, json

from time import time

def increment(t, mod, fixations=None):
	if fixations is None:
		fixations = tuple()

	for i in reversed(xrange(len(t))):
		if i in fixations:
			if isinstance(fixations[i], int):
				floor, ceil = fixations[i], fixations[i]
			else:
				floor, ceil = fixations[i][0], fixations[i][1]
		else:
			floor, ceil = 0, mod

		if t[i] + 1 >= ceil:
			t[i] = floor
		else:
			t[i] += 1
			return t
	else:
		return None

#Q is modulus, M is message length
def allMessages(Q, M, fixations=None, start=None):
	t = start or [0]*M
	if fixations:
		for p in fixations:
			fix = fixations[p] if isinstance(fixations[p], int) else fixations[p][0]
			if p < M:
				t[p] = fix
			else:
				raise StopIteration
	
	while t is not None:
		yield t and t[:]
		t = increment(t, Q, fixations)

#modular arithmetic, constrant trick, horner method
def mth(N, M):
	V = set(xrange(N))
	pa = []

	prev = -1
	for a in allMessages(N, M-1):
		if len(pa) - prev > 1e4:
			prev = len(pa)
			print prev

		codeword = []
		for t in xrange(1, N+1):
			cell = 0
			#horner
			for i in xrange(1, M):
				cell = (cell + a[i-1]) * t
				#~ cell += a[i-1] * pow(t, i, N)
			codeword.append(cell % N)

		if set(codeword) == V:
			for x in xrange(N):
				pa.append([(e+x)%N for e in codeword])
	return pa

#modular arithmetic with trick, horners, and d&c
def mthdc(N, M):
	pa = []
	print 'M is', M
	V = set(xrange(N))
	A1 = []
	print 'M/2 is', M/2
	for a in allMessages(N, M/2):
		codeword = []
		for t in xrange(1, N+1):
			cell = 0
			#SUM[ (a-sub-i) t^(i-1) | 1 <= i <= floor(m/2) ]
			for i in xrange(1, M/2+1):
				cell = (cell + a[i-1]) * t
			codeword.append(cell % N)
		A1.append(codeword)
	print 'Generated A1', len(A1)

	M2 = M/2 - 1 if M%2==0 else M/2
	print 'M2 is', M2
	A2 = []
	for a in allMessages(N, M2):
		codeword = []
		for t in xrange(1, N+1):
			cell = 0
			#SUM[ (a-sub-i) t^(i-1) | floor(m/2) < i <= M ]
			for i in xrange(M2):
				cell = cell * t + a[i]

			#If M is odd, Cell is SUM[ (a-sub-i) t^(i-1) | 0 <= i < floor(M/2)  ]
			#If M is even, Cell is SUM[ (a-sub-i) t^(i-1) | 0 <= i < M/2 - 1  ]
			cell = (cell%N) * pow(t, M/2+1, N) % N
			codeword.append(cell)
		A2.append(codeword)
	print 'Generated A2', len(A2)

	prev = -1
	for A in A1:
		for B in A2:
			if len(pa) - prev > 1e4:
				prev = len(pa)
				print prev

			r = [False]*N
			codeword = []
			for a, b in zip(A, B):
				c = (a+b)%N
				if r[c]:
					break
				else:
					r[c] = True
					codeword.append(c)
			else:
				for x in xrange(N):
					pa.append([(e+x)%N for e in codeword])

	print len(pa), 'done'
	return pa

#group arithmetic, with trick, and horner method
def gth(N, M):
	sofar = 0
	pa = []
	past = 0
	f = GaloisField(N)
	NM = N**(M-1)
	try:
		for o, a in enumerate(allMessages(N, M-1)):
			if len(pa) - past > 1e5 or o == 0:
				past = len(pa)
				print '%3.2f %10d' % (float(o) / NM, sofar + past)
			if len(pa) >= 1e7:
				print 'Writing buffer...'
				sofar += len(pa)
				with open('M_%d_%d_buffer.txt' % (N, N-M+1), 'a') as buf:
					for perm in pa:
						buf.write(pp(perm) + '\n')
				pa = []
				past = 0

			res = [False]*N
			codeword = []

			for t in xrange(N):
				cell = f(0)
				#horner
				for i in xrange(M-1):
					#~ cell = cell*f(t) + f(a[i])
					cell = (cell + f(a[i])) * f(t)

				if res[int(cell)]:
					break
				else:
					res[int(cell)] = True
					codeword.append(cell)
			else:
				for x in xrange(N):
					fx = f(x)
					pa.append([int(e + fx) for e in codeword])
	except KeyboardInterrupt:
		print 'Stopped'

	return pa

#group arithmetic, with trick, and horner method, and divide and conquer
def gthdc(N, M):
	pa = []
	f = GaloisField(N)

	print 'M is', M
	A1 = []
	print 'M/2 is', M/2
	for a in allMessages(N, M/2):
		codeword = []
		for t in xrange(N):
			cell = f(0)
			#SUM[ (a-sub-i) t^(i-1) | 1 <= i <= floor(m/2) ]
			for i in xrange(1, M/2+1):
				cell = (cell + f(a[i-1])) * f(t)
			codeword.append(cell)
		A1.append(codeword)
	print 'Generated A1', len(A1)

	M2 = M/2 - 1 if M%2==0 else M/2
	print 'M2 is', M2
	A2 = []
	for a in allMessages(N, M2):
		codeword = []
		for t in xrange(N):
			cell = f(0)
			#SUM[ (a-sub-i) t^(i-1) | floor(m/2) < i <= M ]
			for i in xrange(M2):
				cell = cell * f(t) + f(a[i])

			#If M is odd, Cell is SUM[ (a-sub-i) t^(i-1) | 0 <= i < floor(M/2)  ]
			#If M is even, Cell is SUM[ (a-sub-i) t^(i-1) | 0 <= i < M/2 - 1  ]
			cell = cell * (f(t) ** (M/2+1))
			codeword.append(cell)
		A2.append(codeword)
	print 'Generated A2', len(A2)

	sofar = 0
	try:
		prev = -1
		prev2 = 0
		for i, A in enumerate(A1):
			for j, B in enumerate(A2):
				if len(pa) - prev > 1e4 or int(float(i)/len(A1))/100 > prev2:
					prev = len(pa)
					prev2 = int(float(i)/len(A1))/100
					print '%3.2f %10d' % (float(i) / len(A1), sofar+ prev)

				if len(pa) >= 1e7:
					print 'Writing buffer...'
					sofar += len(pa)
					with open('M_%d_%d_buffer.txt' % (N, N-M+1), 'a') as buf:
						for perm in pa:
							buf.write(pp(perm) + '\n')
					pa = []
					prev = 0

				r = [False]*N
				codeword = []
				for a, b in zip(A, B):
					c = (a+b)
					if r[int(c)]:
						break
					else:
						r[int(c)] = True
						codeword.append(c)
				else:
					for x in xrange(N):
						fx = f(x)
						pa.append([int(e+fx) for e in codeword])
	except KeyboardInterrupt:
		print 'Stopped'
		pass

	print len(pa), 'done'
	return pa

def exclude_naive(block, E):
	N = len(block[0])
	pa = []
	for perm in block:
		for e in xrange(E):
			if perm[e] < N-E:
				break
		else:
			pa.append(perm)
	return pa

#bereg's algorithm with no d/c, but bells and whistles
def bereg(q, m, fixations=None, verbose=False, field=None, startJ=None, startX=None, perms=None, savefile=None, prim=None): #q=N, m=M
	if verbose:
		print 'Message length of', m
	f = field or GaloisField(q, prim=prim)
	print 'Using primitive polynomial with coefficients: ', f.prim.coef

	ret = perms or []
	last = time()
	filename = savefile or "M_%d_%d_%s.xsav" % (q, q-m+1, timestamp())
	with open(filename, 'a+') as sav:
		if not savefile:
			sav.write("#A safe mode log file for xrs.py\n")
			sav.write("Q=%d\n" % q)
			sav.write("M=%d\n" % m)
			sav.write("FIXATIONS=" + json.dumps(fixations) + '\n')
			sav.write("PRIM=" + json.dumps(f.prim.coef) + '\n')
			sav.write("VERBOSE=" + str(verbose) + '\n')
			sav.write("#"*10 + '\n')
			sav.flush()
		for j in xrange(startJ or 1, m):
			for x in allMessages(q, m-j-1, fixations, j==startJ and startX):
				if time() - last > 1:
					sav.write("JX=%d@%s\n" % (j, json.dumps(x)))
					sav.flush()
					last = time()

				x = x + [1]# + [0]*j
				code = []
				r = [False]*q
				for t in xrange(q):
					p = f(0)
					for e in x:
						p = p*f(t) + f(e)
					p = int(p * pow(f(t), j))
					if r[p]:
						break
					else:
						r[p] = True
						code.append(p)
				else:
					if verbose:
						print 'Msg: ', x + [0]*j
						print 'Code:',code
						print
					if sav:
						sav.write("PERM=" + json.dumps(code) + "\n")
						sav.write("JX=%d@%s\n" % (j, json.dumps(x)))
						sav.flush()
						last = time()
					ret.append(code)

		sav.write("DONE\n")
	return f, ret

#bereg's algorithm with divide and conquer
def bereg_dc(N, M, verbose=False, prim=None): #q=N, m=M
	f = GaloisField(N, prim=prim)
	print 'Using primitive polynomial with coefficients: ', f.prim.coef

	ret = []
	count = 0

	#message length is m-j-1, plus a 1
	#since j goes from 1 to m-1, that means message length goes
	#from m-2, m-3, m-4, ..., 1, 0.
	#so let's go backwards, and let message length be k
	#thus m-j-1 = k, or j = m-k-1

	for k in xrange(min(2, M-1)):
		#~ print 'k is', k#, len(ret)
		for x in allMessages(N, k):
			code = []
			r = [False]*N
			for t in xrange(N):
				p = f(0)
				for e in x:
					p = p*f(t) + f(e)
				p = p*f(t) + f(1)
				p = int(p * pow(f(t), M-k-1))
				if r[p]:
					break
				else:
					r[p] = True
					code.append(p)
			else:
				ret.append(code)


	print len(ret)*N*(N-1)
	for k in xrange(2, M-1):
		#~ print 'k is', k#, len(ret)
		A1 = []
		#~ print 'k/2 is', k/2
		for a in allMessages(N, k/2):
			codeword = []
			for t in xrange(N):
				cell = f(0)
				#SUM[ (a-sub-i) t^(i-1) | 0 <= i < floor(m/2) ]
				for i in xrange(k/2):
					cell = cell * f(t) + f(a[i])
				codeword.append(cell)
			A1.append(codeword)
		#~ print 'Generated A1', len(A1)

		k2 = k - k/2
		#~ print 'k2 is', k2
		A2 = []
		for a in allMessages(N, k2):
			codeword = []
			for t in xrange(N):
				cell = f(0)
				#SUM[ (a-sub-i) t^(i-1) | floor(m/2) <= i < k ]
				for i in xrange(k2):
					cell = cell * f(t) + f(a[i])
				cell = cell * (f(t) ** (k/2))
				codeword.append(cell)
			A2.append(codeword)
		#~ print 'Generated A2', len(A2)

		prev = time()
		for i, A in enumerate(A1):
			for j, B in enumerate(A2):
				if time() - prev > 1:# or percent > prev2:
					prev = time()
					print len(ret), len(ret)*N*(N-1)

				r = [False]*N
				codeword = []
				for t in xrange(N):
					c = int(((A[t] + B[t]) * f(t) + f(1)) * f(t)**(M-k-1))
					if r[c]:
						break
					else:
						r[c] = True
						codeword.append(c)
				else:
					ret.append(codeword)
	return f, ret


'''
Also, there is another form of Reed-Solomon code that is referred to (poorly) in the Iranian paper and I've now done some reading about.  In this case one takes a "generator" polynomial, say g(x) = Product( (x-a^j) | j = 1,2, ... , q-k ), where q is a power of a prime, and one more than the codeword length, k is the message word length, and "a" is a primitive element, such as "2", among the non-zero elements of GF(q), i.e. the powers of a give all of the non-zero elements of GF(q). For example, if we take q = 5, k = 3, and a=2, then we get g(x) = (x-2)(x-4) = x^2 -6x +8 = x^2 -x +3 (using mod(5) arithmetic).

A message (m(0), m(1), ... , m(k-1) is viewed as the coefficients of a degree k-1 polynomial, i.e. m(x) = m(0) + m(1)x + m(2)x^2 + ... + m(k-1)x^(k-1). 

The codeword produced, say c(x), is the sequence of coefficients of the product of m(x) and g(x). The product will be a polynomial of degree q-1, as it is the product of a degree k-1 polynomial and a degree q-k polynomial.

For example, if m(x) = 2x^2 + x + 1, then (for the example generator polynomial g(x) given above) one would get the codeword (2x^2 + x + 1)(x^2 -x +3) = 2x^4 - x^3 + 6x^2 + 2x + 3 = 2x^4 - x^3 + x^2 + 2x + 3 (with mod 5 arithmetic).

This alternative Reed-Solomon code may give something new for cases that are one less than a power of a prime. Maybe you can write a program to check it out? Again, the basic idea is to look at all possible message words and see which ones produce permutations. As before we can probably speed this search up a bit by making the low order message symbol, i.e. m(0), zero. That is, if that's a permutation then so are the results when m(0) = 1,2,3, ...

'''

def isPermutation(arr):
	r = [False]*len(arr)
	for e in arr:
		if e < 0 or e >= len(r) or r[e]:
			return False
		else:
			r[e] = True
	return True

def alt_naive(q, d, verbose=False):
	#q is a power of a prime, one more than the codeword length
	#k is the message length
	#a is a primitive element
	for a in xrange(2, q):
		r = [False]*(q-1)
		pot = a
		for _ in xrange(q-1):
			if r[pot-1]:
				break
			else:
				r[pot-1] = True
			pot = (pot*a)%q
		else:
			break
	else:
		print 'No primitive element'
		exit(1)

	#~ if verbose:
	print 'Primitive element:', a

	#the "dimension" of this version of RS codes is q-2t-1
	#where one chooses 2t powers of the generating element "a" in making the generating polynomial
	#And, I believe the dimension corresponds to Hamming distance

	#g(x) is a generator matrix
	setModulus(q)

	g = Polynomial([1])
	X = Polynomial([0, 1])
	for j in xrange(1, d-1):
		g *= X - Polynomial([a**j % q])

	print 'g(x):', g

	#~ m = Polynomial([1, 1, 2])
	#~ print 'm(x)', m
	#~ print 'codeword', g*m

	#
	k = q-d+1
	ret = []
	for message in allMessages(q, k):
		m = Polynomial(message)
		cp = m*g
		cw = zeropad(cp.coef, q-1)

		#~ print 'Message: ', m.coef
		#~ print 'Codeword:', cw

		if isPermutation(cw):
			ret.append(cw)
			if verbose:
				print 'Message: ', zeropad(m.coef, k)
				print 'Codeword:', cw
				print len(ret)
				print
	return ret

def doTheDumbThing(d):
	ret = dict()
	for k in d:
		ret[int(k)] = d[k]
	return ret


#new savefile mode
def beregParse(filename):
	try:
		perms = []
		j, x = 0, 0
		q, m, fixations, field, verbose = [None]*5
		with open(filename, 'r') as f:
			for line in f:
				if line.startswith('#'):
					continue
				if line.startswith("DONE"):
					break
				pre, suf = line.strip().split('=')

				if pre == 'Q':
					q = int(suf)
				elif pre == 'M':
					m = int(suf)
				elif pre == 'FIXATIONS':
					fixations = doTheDumbThing(json.loads(suf))
				elif pre == 'PRIM':
					field = GaloisField(q, prim=list(json.loads(suf)))
				elif pre == 'VERBOSE':
					verbose = suf == 'True'
				elif pre == 'PERM':
					perms.append(list(json.loads(suf)))
				elif pre == 'JX':
					a, b = suf.split('@')
					j = int(a)
					x = list(json.loads(b))
	except:
		return None

	if q is None or m is None:
		return None

	print 'Parsed. Running naive bereg mode'
	print q, m, fixations, field, verbose, j, x, len(perms)
	f, pa = bereg(q, m, fixations=fixations, verbose=verbose, field=field, startJ=j, startX=x, perms=perms, savefile=filename)
	return q, q-m+1, f, pa

# group arithmetic, with trick, horner method, and restrictions
def gthr(N, D, fixations, verbose=False, prim=None, startingMessage = None, sofar=0):
	# permutationPolynomials = []
	f = GaloisField(N, prim=prim)
	print 'Using primitive polynomial with coefficients: ', f.prim.coef

	filebase = 'PP_%d_%d' % (N, D)
	for k in sorted(fixations.keys()):
		filebase += '_%d:%s' % (k, sliceFormat(fixations[k]))
	backup_filename = filebase + '.bup'
	filename = filebase + '.txt'

	try:
		pps = []
		last_time = time()
		#START all messages for loop
		for msg_idx, msg in enumerate(allMessages(N, D, fixations, startingMessage)):
			if verbose:
				print 'Message %d: %s' % (msg_idx, msg + [0])
			if time() - last_time > 60:
				with open(filename, 'a') as buf:
					for p in pps:
						buf.write(pp(p+[0]) + '\n')

				with open(backup_filename, 'w+') as bup:
					record = {
						'n': N,
						'd': D,
						'fixations': fixations,
						'verbose': verbose,
						'primitivePolynomial': f.prim.coef,
						'startingMessage': msg,
						'found': sofar
					}

#					print 'Backing up record: %s' % record
					json.dump(record, bup)
				pps = []
				last_time = time()

			#here we calculate the codeword's elements.
			elementLut = [False] * N
			codeword = []

			#the t'th element of the codeword will be calculated on iteration t
			for t in xrange(N):
				''' horner's method to calculate P(t).
					Note that my coefficients are backwards, but now a standard has been created
					and it would be harder to change all the other functions to comply than it would
					be to document this quirk. I will change it later if I find the time.
					
					The quirk is this: When evaluating P(t), the polynomial with coefficients given by msg,
					say we have a degree D polynomial. Then msg[i] is the coefficient of x ^ (D - i)
					thus msg[0] is for x^D and msg[D-1] is for x^1.
					
					In this function, we use the so called "trick" so x^0 always has coefficient 0.
				'''
				cell = f(0)
				for i in xrange(D):
					cell = (cell + f(msg[i])) * f(t)

				if elementLut[int(cell)]:
					break
				else:
					elementLut[int(cell)] = True
					codeword.append(cell)
			else:
				pps.append(msg)
				s = pp(msg + [0])
				sofar += 1
				print '%d: %s' % (sofar, s)

		#END all messages for loop
		print 'Calculations complete'

	except KeyboardInterrupt:
		print 'Calculations halted'

	print 'In total, %d polynomials were found.' % sofar
	return True

#this function is borrowed from
# https://stackoverflow.com/questions/45068797/how-to-convert-string-int-json-into-real-int-with-json-loads
def _decode(o):
    if isinstance(o, str) or isinstance(o, unicode):
        try:
            return long(o)
        except ValueError:
            return o
    elif isinstance(o, dict):
        return {_decode(k): _decode(v) for k, v in o.items()}
    elif isinstance(o, list):
        return [_decode(v) for v in o]
    else:
        return o


def polyParse(filename):
	with open(filename, 'r') as f:
		try:
			doc = json.load(f, object_hook=_decode)
		except:
			return None
	N = doc.get('n', None)
	D = doc.get('d', None)
	fixations = doc.get('fixations', None)
	verbose = doc.get('verbose', None)
	prim = doc.get('primitivePolynomial', None)
	startingMessage = doc.get('startingMessage', None)
	found = doc.get('found', None)

	gthr(N, D, fixations, verbose=verbose, prim=prim, startingMessage=startingMessage, sofar=found)

def sliceParse(s):
	try:
		return int(s)
	except:
		return map(int, s.split(':'))

def sliceFormat(s):
	if isinstance(s, int):
		return str(s)
	else:
		return ':'.join(map(str, s))

from sys import argv

if __name__ == '__main__':
	#no args
	if len(argv)-1 < 1:
		print HELPSTR
		exit(1)

	#start calculation
	elif len(argv)-1 > 1:
		P, R = parsePR(argv[1])
		N = P**R
		D = int(argv[2])
		FIXATIONS = {}
		NAIVE = False
		VERBOSE = False
		PP = False
		ALT = False
		PRIM = []

		i = 2
		while i+1 < len(argv):
			i += 1
			e = argv[i]
			if e.lower() in ('-a', '--alt'):
				ALT = True
			elif e.lower() in ('-n', '--naive'):
				NAIVE = True
			elif e.lower() in ('-v', '--verbose'):
				VERBOSE=True
			elif e.lower() in ('-p', '--polynomial'):
				PP=True
			elif e.lower() in ('-r', '--primitive'):
				while i+1 < len(argv):
					i += 1
					try:
						PRIM.append(int(argv[i]))
					except:
						break
			elif not i&1:
				try:
					slice = sliceParse(argv[i+4])
					if isinstance(slice, int) or len(slice) == 2:
						FIXATIONS[int(e)] = slice
					else:
						print 'Invalid arguments'
						exit(1)
				except:
					print 'Invalid arguments'
					exit(1)

		PRIM = PRIM or None

		if PP:
			print 'Permutation Polynomial mode'
			print 'We will generate all polynomials of degree %d with coefficients in GF(%d)' % (D, N)
			print 'fixing the following coefficients:'
			print '0 * x^0'
			for k in reversed(sorted(FIXATIONS.keys())):
				print '%s * x^%d' % (sliceFormat(FIXATIONS[k]), D-k)
			print 'Starting calculation!'
			gthr(N, D, FIXATIONS, verbose=VERBOSE, prim=PRIM)
			exit(0)

		if ALT:
			if PRIM:
				print 'Cannot specify a primitive polynomial in alt mode. Exiting.'
				exit(1)

			print 'Alternative mode'
			pa = alt_naive(N, D, VERBOSE)

			print 'Found %d permutations' % len(pa)
			if raw_input('Output to file? (Y/n):') != 'n':
				with open('M_%d_%d_%d.txt' % (N-1, D, len(pa)), 'w+') as f:
					for perm in pa:
						f.write(pp(perm) + '\n')

			exit(0)

		if NAIVE or FIXATIONS:
			print 'Naive Bereg mode with fixations:', FIXATIONS
			field, pa = bereg(N, N-D+1, fixations=FIXATIONS, verbose=VERBOSE, prim=PRIM)
		else:
			print 'Divide and Conquer Bereg Mode'
			field, pa = bereg_dc(N, N-D+1, VERBOSE, prim=PRIM)

	#resume
	else:
		print 'Parsing .xsav file...'

		#try bereg mode's file parsing
		bp = beregParse(argv[1])
		if bp:
		   exit(0)
		else:
			pp = polyParse(argv[1])
			if pp:
				exit(0)

		print 'Invalid save file.'
		exit(1)


	print 'M(%d, %d) >= %d' % (N, D, len(pa)*N*(N-1))
	if raw_input('Output to file? (Y/n):') != 'n':
		#~ print len(pa), len(pa)*N*(N-1), 'done'
		filename = "M_%d_%d_%d.xtar" % (N, D, len(pa)*N*(N-1))
		xtar.compress(filename,
					  filetype='cosets',
					  pa=xtar.compresspa(type='agl', field=field),
					  reps=pa)

