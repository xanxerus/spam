
from xutils.xfield import GaloisField
from xoddsud import replicate
import xtar

def makepis(N, R, piP, piQ, f):
	NR = N**R
	
	K = len(piP)
	S = NR / (K+1)

	#replication
	Plv1s = []
	Qlv1s = []
	for s in xrange(S):
		Plv1s.append(replicate(piP, s*(K+1), NR))
		Qlv1s.append(piQ)

	return Plv1s, Qlv1s, f

if __name__ == '__main__':
	import json
	from sys import argv
	
	with open(argv[1], 'r') as infile:
		data = json.load(infile)
		N = data['pa']['n']
		R = data['pa']['r']
		piP = data['P']
		piQ = data['Q']
		f = GaloisField(N, R, data['pa']['prim'])

		Plv1s, Qlv1s, f = makepis(N, R, piP, piQ, f)

		filename = 'Pis_%d.xtar' % (N**R+1)
		
		xtar.compress(filename,
						   filetype='pi',
						   pa=xtar.compresspa(type='agl', field=f),
						   P1=map(list, Plv1s),
						   Q1=map(list, Qlv1s))