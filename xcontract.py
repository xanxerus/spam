#!/usr/bin/pypy -i
from xutils.xfield import GaloisField, GaloisElement, allPrims, setModulus
from xutils import parsePR
from threading import Thread
from sys import stdout
import xtar

#~ from xverify import naive_hd, join_pa

HELPSTR = '''
xcontract.py will do its best to directly construct an independent
set within one connected component, then use isomorphism and isolated
points to construct a permutation array for M(N^R, N^R-3)

Usage:
	python xcontract.py Q [OPTIONS]

Where:
	Q is either a prime, a prime power, or a string of the form:
		P^R, where P is prime and R is a prime power
		and Q is congruent to 1 (mod 3)

Options:
	-v, --verbose, Print extra information
	-t, --tryhard, Try all primitive polynomials
	-w, --write, will write to file without prompting

'''.strip()

def kitty(Q, c):
	return c*(Q-1) + Q*(Q-1)

def onlinecontractedpgl(f, dep, markers=False):
	for k in xrange(1, f.Q):
		for a, i in dep:
			yield contractedpglperm(f, int(f(k)*f(a)), i, k)
		if markers:
			yield None

	for a in xrange(1, f.Q):
		for b in xrange(f.Q):
			perm = []
			for x in xrange(f.Q):
				perm.append(int(f(a) * f(x) + f(b)))
			yield perm
	if markers:
		yield None

def contractedpglperm(f, a, i, k=1):
	ret = []
	for x in xrange(f.Q):
		if x != i:
			ret.append(int(f(a) + f(k) / (f(x) - f(i))))
		else:
			ret.append(a)
	return ret

def noncontractedpglperm(f, a, i, k=1):
	ret = []
	for x in xrange(f.Q):
		if x != i:
			ret.append(int(f(a) + f(k) / (f(x) - f(i))))
		else:
			#~ print Q,
			ret.append(Q)
	ret.append(a)
	return ret

#mutates graph
def heuristic7(edges):
	dep = set()
	while edges and any(edges.values()):
		#pick the u with the least number of neighbours
		u = None
		for pot in edges:
			if u is None or len(edges[pot]) < len(edges[u]):
				u = pot
		
		#add to independent set
		dep.add(u)
		
		#delete all vertices connected to u
		for e in edges[u]:
			for g in edges[e]:
				if g != u:
					edges[g].discard(e)
			
			#~ if e in edges:
			del edges[e]
		del edges[u]

	return dep

def generateGraph(f):
	edges = dict()
	for a in xrange(f.Q):
		for i in xrange(f.Q):
			edges[(a, i)] = set()
			for j in xrange(f.Q):
				if i != j:
					edges[(a, i)].add(((f(a) - (f(i) - f(j)).inv()).index, j))
	return edges

def independentSetToPA(f, dep):
	pa = []
	for k in xrange(1, f.Q):
		subblock = []
		for a, i in dep:
			perm = contractedpglperm(f, int(f(k)*f(a)), i, k)
			subblock.append(perm)
		pa.append(subblock)

	subblock = []
	for a in xrange(1, f.Q):
		for b in xrange(f.Q):
			perm = []
			for x in xrange(f.Q):
				perm.append(int(f(a) * f(x) + f(b)))
			subblock.append(perm)
	pa.append(subblock)
	return pa

from sys import argv

if __name__ == '__main__':
	if len(argv) not in (2, 3, 4):
		print HELPSTR
		exit(1)

	#parse CLI args
	N, R = parsePR(argv[1])
	
	VERBOSE, TRYHARD, WRITE = False, False, False
	for e in argv[2:]:
		if e in ('-v', '--verbose'):
			VERBOSE = True
		elif e in ('-t', '--tryhard'):
			TRYHARD = True
		elif e in ('-w', '--write'):
			WRITE = True

	if N**R % 3 != 1:
		print '%d is not congruent to 1 (mod 3)' % (N**R)
		exit(1)
	
	if TRYHARD:
		print 'Trying every primitive polynomial!'
		if not WRITE:
			print 'Press enter to stop early.'
			thread = Thread(target=raw_input)
			thread.start()
		best = 0
		bestprim = None
		dep = None
		print '%-10s %-10s %-10s %-30s' % ('Best', 'I.Set', 'M(Q, Q-3)', 'Polynomial')

		for f in GaloisField.allFields(N, R):
			if not WRITE and not thread.is_alive():
				if raw_input('Really stop? (y/N): ') == 'y':
					break
				else:
					thread.join()
					thread = Thread(target=raw_input)
					thread.start()
			
			edges = generateGraph(f)
			pot = heuristic7(edges)
			#~ del f, edges

			if dep is None or len(pot) > len(dep):
				dep = pot
				bestprim = f.prim
				best = kitty(f.Q, len(pot))
				print '%-10s %-10d %-10s %-10s*' % (best, len(pot), kitty(f.Q, len(pot)), f.prim)
			elif VERBOSE:
				print '%-10s %-10d %-10s %-10s' % (best, len(pot), kitty(f.Q, len(pot)), f.prim)
		
		#~ #the thread is still alive.
		if not WRITE and thread.is_alive():
			stdout.write("Press enter because I'm a bad programmer: ")
			stdout.flush()
			while thread.is_alive():
				pass
		
		f = GaloisField(N, R, bestprim)
		
	else:
		f = GaloisField(N, R)
		if VERBOSE:
			print 'Using Polynomial:', f.prim
		edges = generateGraph(f)
		dep = heuristic7(edges)
	
	#~ pa = independentSetToPA(f, dep)
	print 'Independent set of size %d' % (len(dep))
	print 'M(%d, %d) >= %d' % (f.Q, f.Q-3, kitty(f.Q, len(dep)))
	#~ print list(dep)
	if WRITE or raw_input('Write to file? (Y/n): ') != 'n':
		print 'Writing to file'
		filename = 'M_%d_%d_%d.xtar' % (f.Q, f.Q-3, kitty(f.Q, len(dep)))
		xtar.compress(filename, filetype='contraction', field=f, dep=list(dep))
