#!/usr/bin/pypy -i

from xutils import parse_pa, join_pa, pp
from xtar import extract

from itertools import combinations
from sys import argv
import json

HELPSTR = '''
xrestrict.py tries to reduce a PA on N symbols by R symbols, creating
a PA on N-R symbols.

Usage:
	python xrestrict.py FILENAME R

Where:
	FILENAME is the filename of a PA, either .txt or .xtar
	R is the number of symbols by which to restrict the PA

'''.strip()

if __name__ == '__main__':
	if len(argv) < 3:
		print HELPSTR
		exit(1)
	
	filename = argv[1]
	R = int(argv[2])

	if filename.endswith('xtar'):
		with open(filename, 'r') as f:
			block = extract(mode='ram', **json.load(f))
	else:
		block = parse_pa(filename)

	block = join_pa(block)
	print 'Given %d permutations...' % len(block)
	N = len(block[0])

	pot = 0
	for perm in block:
		k = sorted(perm[-R:])
		if k == range(N-R, N):
			pot += 1

	print 'Maintained %d permutations' % pot
	if raw_input('Print to file? (Y/n): ') != 'n':
		filename = raw_input('Filename: ')
		with open(filename, 'w+') as f:
			for perm in block:
				k = sorted(perm[-R:])
				if k == range(N-R, N):
					f.write(pp(perm[:-R]) + '\n')
	
