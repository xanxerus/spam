#!/usr/bin/env pypy

from xutils.xfield import GaloisField
from xutils import parsePR

HELPSTR = '''

xgroup.py will generate AGL or PGL.

Usage:
	python xgroup.py GROUP Q [K]

Where:
	GROUP is either agl or pgl or modagl
	Q is either a prime, a prime power, or a string of the form:
		P^R, where P is prime and R is a prime power
	K is the number of cosets to use, if using AGL

'''

from sys import argv

def onlinemodagl(N, K, markers=False):
	for a in xrange(1, min(N-1, K)+1):
		for b in xrange(N):
			yield [(a*x + b)%N for x in xrange(N)]
		if markers:
			yield None

def onlinegroupagl(f, K=None, markers=False):
	if K is None or K > f.Q-1:
		K = f.Q-1
	for a in xrange(1, K+1):
		for b in xrange(f.Q):
			yield [int(f(a)*f(x) + f(b)) for x in xrange(f.Q)]
		if markers:
			yield None

def onlinepgl(f, markers=False):
	#each connected component
	for k in xrange(1, f.Q):
		for a in xrange(f.Q):
			for i in xrange(f.Q):
				#print for all x in the field
				perm = []
				for x in xrange(f.Q):
					if x != i:
						perm.append(int(f(a) + f(k) / (f(x) - f(i))))
					else:
						perm.append(f.Q)
				perm.append(a)
				yield perm
		if markers:
			yield None

	#isolated points
	for a in xrange(1, f.Q):
		for b in xrange(f.Q):
			perm = []
			for x in xrange(f.Q):
				perm.append(int(f(a) * f(x) + f(b)))
			perm.append(f.Q)
			yield perm
	
	if markers:
		yield None
		

def onlinecomposecosets(onlinepa, reps):
	from xutils import compose
	for perm in onlinepa:
		if perm:
			for r in reps:
				yield compose(r, perm)
		else:
			yield None

if __name__ == '__main__':
	import xtar

	if len(argv) in (3, 4):
		GROUP = argv[1].lower()
		P, R = parsePR(argv[2])
		
		if GROUP == 'modagl':
			if R != 1:
				print 'modagl is only for plain primes.'
				exit(1)
			
			if len(argv)==4 and int(argv[3]) < P**R:
				xtar.compress('AGL_%d_%dcosets.xtar' % (P,  int(argv[3])), filetype='pa', 
					pa=xtar.compresspa(type='modagl', n=P, k=int(argv[3])))
			else:
				xtar.compress('AGL_%d.xtar' % (P,), filetype='pa', 
					pa=xtar.compresspa(type='modagl', n=P, r=R))
		elif GROUP == 'agl':
			prim = GaloisField.randomPrim(P, R).coef
			if len(argv)==4 and int(argv[3]) < P**R:
				xtar.compress('AGL_%d^%d_%dcosets.xtar' % (P, R, int(argv[3])), filetype='pa',
					pa=xtar.compresspa(type='agl', prim=prim, n=P, r=R, k=int(argv[3])))
			else:
				xtar.compress('AGL_%d^%d.xtar' % (P, R), filetype='pa', 
					pa=xtar.compresspa(type='agl', prim=prim, n=P, r=R))
		elif GROUP == 'pgl':
			prim = GaloisField.randomPrim(P, R).coef
			xtar.compress('PGL_%d^%d.xtar' % (P, R), filetype='pa',
				pa=xtar.compresspa(type='pgl', prim=prim, n=P, r=R))
		else:
			print '%s is not a valid group type.' % GROUP
	else:
		print HELPSTR
