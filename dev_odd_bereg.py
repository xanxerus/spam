#!/usr/bin/env pypy

from xutils.xfield import GaloisField
from xutils.xpe import extend_coset


class OddBereg:
	def __init__(self, p, r, verbose=False, quiet=False):
		self.p = p
		self.r = r
		self.k = self.r / 2
		self.q = self.p**self.r
		self.f = GaloisField(self.p, self.r)
		self.verbose = verbose
		self.quiet = quiet and not verbose
		self.queue = []

		if not self.quiet:
			print 'Galois field of (%d ^ %d)' % (p, r)
			print 'Prim: ', self.f.prim
			print 'Generating lookup tables...'
			self.f.calctadd()
			self.f.calctmul()
			print 'Lookup complete'
			if self.verbose:
				for e in self.f.elements:
					print int(e), e.coef
				print

	def positions_V(self, V):
		if self.verbose:
			print 'Generating positions with suffix', V

		ret = []
		for u_prefix in OddBereg.alltuples(self.p, self.k+1):
			ret.append(int(self.f(u_prefix + V)))
			if self.verbose:
				print ret[-1], u_prefix, V

		if self.verbose:
			print
		return ret

	def symbols_V(self, V):
		if self.verbose:
			print 'Generating symbols with suffix', V
	
		ret = []
		for u_suffix in OddBereg.alltuples(self.p, self.k+1):
			ret.append(int(self.f(V + u_suffix)))
			if self.verbose:
				print ret[-1], V, u_suffix

		if self.verbose:
			print
		return ret

	def in_symbols_V(self, q, V):
		return q[:self.k] == V

	def V_V(self, l):
		ret = []
		for _ in xrange(self.k):
			ret.append(l%self.p)
			l /= self.p
		return tuple(ret)

	@staticmethod
	def alltuples(p, k):
		if k <= 0:
			yield tuple()
		else:
			for pre in OddBereg.alltuples(p, k-1):
				for e in xrange(p):
					yield pre + (e,)

	def enqueue(self, perm, u):
		if u < 0:
			extended = ' '.join(map(str, map(int, perm[:] + (self.q,))))
		else:
			extended = ' '.join(map(str, map(int, perm[:u] + (self.q,) + perm[u+1:] + perm[u:u+1])))

		self.queue.append(extended)

	def dump(self, outfile):
		with open(outfile, 'w+') as of:
			of.write("#%d %s\n" % (self.q, self.f.prim.coef))
			for perm in self.queue:
				of.write(perm + '\n')
			self.queue = []

	def cosetCount(self, l):
		V = self.V_V(l)
		P_V = self.positions_V(V)

		if self.verbose:
			print 'Coset %d:' % l
			print 'V:', V
			print 'Positions: ', P_V
			print 'Symbols: ', self.symbols_V(V)

		# coset loop
		a = self.f((1,) + V + (0,)*(self.k))
		count = 0
		for b in self.f.elements:

			# generate permutation
			perm = tuple(a*self.f(x)+b for x in xrange(self.q))

			# cover permutation
			covered = -1
			for u in P_V:
				if self.in_symbols_V(perm[u].coef, V):
					covered = u
					break

			# if covered, enqueue
			if covered > -1:
				self.enqueue(perm, u)
				count += 1

		if not self.quiet:
			print 'Coset %d coverage: %d / %d' % (l, count, self.q)
			if self.verbose:
				print
				print
				print

		return count

	def churn(self):
		ret = 0
		for l in xrange(self.p ** (self.k)):
			self.queue.append('\n# coset %d' % l)
			ret += self.cosetCount(l)

		ret += self.freebie()
		return ret

	def freebie(self):
		if not self.quiet:
			print 'Freebie: ', self.q

		self.queue.append('\n# freebie')
		a = self.f((0, 1) + (0,) * (self.r - 2))
		for b in self.f.elements:
			perm = tuple(a*self.f(x)+b for x in xrange(self.q))
			self.enqueue(perm, -1)

		return self.q

if __name__ == '__main__':
	# argument parsing starts here
	import argparse

	parser = argparse.ArgumentParser(
		prog="xspeedybereg.py",
		description="implements Dr. Bereg's even faster technique "
		'which uses residue arguments for good coverage of AGL.')

	parser.add_argument(
		'q', 
		type=str,  
		help='Q an even power of a prime, or a string of the '
			 'form P^R, where P is prime and R is an even number')

	parser.add_argument(
		'-o', '--outfile',
		type=str,
        help='File to output. Default: {Q+1}_{Q}_sbereg.txt')

	parser.add_argument(
		'-v', '--verbose',
		action='store_true',
        help='Verbose output. Default: false')

	parser.add_argument(
		'-q', '--quiet',
		action='store_true',
        help='Quiet output. Default: false')


	args = parser.parse_args()

	from xutils import parsePR
	N, R = parsePR(args.q)

	if not R & 1:
		print "R must be odd"
		exit(1)


	# Real stuff starts here
	sb = OddBereg(N, R, verbose=args.verbose, quiet=args.quiet)
	count = sb.churn()

	if not args.outfile:
		args.outfile = '%d_%d_%d.txt' % (N**R+1, N**R, count)

	sb.dump(args.outfile)