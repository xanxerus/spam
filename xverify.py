#!/usr/bin/pypy

from xutils.xutils import join_pa, compose, invert, parse_pa
from xutils.xmath import era, isprime
import json
import re

''' Naive HD check for a block B and a distance D
	returns None if hd(B) >= D
	else, returns the indices of two permutations with hd < distance
'''
def naive_hd(block, distance):
	I, N = len(block), len(block[0])
	for i in xrange(I):
		for j in xrange(i+1, I):
			pot = 0
			for p in xrange(N):
				if block[i][p] != block[j][p]:
					pot += 1
			if pot < distance:
				return (i, j)
	return None

''' HD check for a permutation P and a block B and a distance D
	returns None if hd(P, B) >= D
	else, returns the index of the first permutation which failed
		and the hamming distance
'''
def add_hd(perm, block, distance):
	for i, pi in enumerate(block):
		pot = 0
		for p in xrange(len(pi)):
			if pi[p] != perm[p]:
				pot += 1
		if pot < distance:
			return i, pot
	return None

''' Advanced HD check for a block B and its coset representatives
	and a distance D
	
	returns None if hd >= D
	
'''
def coset_hd(block, reps, distance):
	#make sure the first rep is the identity
	for i in xrange(len(reps)):
		for j in xrange(i+1, len(reps)):
			print i, j
			pot = add_hd(compose(invert(reps[j]), reps[i]), block, distance)
			if pot is not None:
				return i, j, pot[0], pot[1]
	return None

def allperms(block, N=0):
	N = N or len(block[0])
	for i, perm in enumerate(block):
		rep = [False]*N
		for q in perm:
			if q >= len(rep) or rep[q]:
				return perm
			else:
				rep[q] = True
	return None

from sys import argv

HELPSTR='''
Usage:
To determine the HD without having a prior guess, try:
	python xverify.py

To verify one PA:
	python xverify.py PA_FILE DISTANCE

To verify a coset search result:
	python xverify.py PA_FILE COSET_FILE DISTANCE

To verify a coset the long way:
	python xverify.py PA_FILE COSET_FILE DISTANCE --long
	
	The --long must go at the end!

'''.strip()

def getblock(filename):
	return join_pa(parse_pa(filename))

if __name__ == '__main__':
	if len(argv) < 2:
		print HELPSTR

	elif len(argv) == 2:
		block = getblock(argv[1])
		pot = allperms(block)
		if pot is not None:
			print 'Row %d is not a permutation' % pot
			exit(0)
		else:
			print 'All rows are permutations'

		N = len(block[0])
		hd = N
		pot = naive_hd(block, hd)
		while pot is not None:
			print 'HD of %d refuted by %d and %d' % (hd, pot[0], pot[1])
			hd -= 1
			pot = naive_hd(block, hd)
		print 'Verified! Hamming distance is:', hd

	elif len(argv) == 3:
		print 'Verifying "%s" with HD %d' % (argv[1], int(argv[2]))
		pot = naive_hd(getblock(argv[1]), int(argv[2]))
		if pot is None:
			print 'Verified!'
		else:
			print 'Refuted! Permutations %d and %d disagree in too few places.' % pot

	elif len(argv) == 4:
		print 'Verifying "%s" with coset reps in "%s" HD %d' % (argv[1], argv[2], int(argv[3]))
		pot = coset_hd(getblock(argv[1]), getblock(argv[2]), int(argv[3]))
		if pot is None:
			print 'Verified!'
		elif len(pot) == 2:
			print 'Refuted! Permutations %d and %d in the original group disagree in too few places' % pot
		else:
			print 'Refuted!', pot

	elif len(argv) == 5:
		print 'Going the long way around.'
		main = getblock(argv[1]) 
		pa = [big_compose(perm, main) for perm in getblock(argv[2])]
		print naive_hd(join_pa(pa), int(argv[3]))
