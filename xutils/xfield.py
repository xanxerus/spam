#!/usr/bin/pypy -i

from random import randrange
from xmath import factorize

'''
Now that there is a GaloisField class and a GaloisElement class,
the Polynomial class is deprecated for use outside this module.
'''
mod = None
class Polynomial:
	def __init__(self, coef):
		if mod is None:
			raise ValueError('Mod is not initialized')

		i = len(coef)-1
		while i >= 0 and coef[i] == 0:
			i -= 1
		
		self.deg = i
		self.coef = coef[:i+1]
	
	@staticmethod
	def zero():
		return Polynomial([])
	
	@staticmethod
	def monomial(n):
		return Polynomial([0]*n + [1])
	
	def __mul__(self, other):
		if self.deg == -1 or other.deg == -1:
			return Polynomial([])
		
		newCoef = [0]*(self.deg + other.deg + 1)
		for i in xrange(len(self.coef)):
			for j in xrange(len(other.coef)):
				newCoef[i+j] = (self.coef[i]*other.coef[j] + newCoef[i+j]) % mod
		return Polynomial(newCoef)

	def __add__(self, other):
		newCoef = [0]*max(len(self.coef), len(other.coef))
		for i in xrange(len(newCoef)):
			if i < len(self.coef):
				newCoef[i] = (newCoef[i] + self.coef[i]) % mod
			if i < len(other.coef):
				newCoef[i] = (newCoef[i] + other.coef[i]) % mod
		
		return Polynomial(newCoef)

	def __sub__(self, other):
		newCoef = [0]*max(len(self.coef), len(other.coef))
		for i in xrange(len(newCoef)):
			if i < len(self.coef):
				newCoef[i] = (newCoef[i] + self.coef[i]) % mod
			if i < len(other.coef):
				newCoef[i] = (newCoef[i] - other.coef[i]) % mod
		
		return Polynomial(newCoef)

	#no idea what this does
	def _divideLeadTerms(self, other):
		temp = self.deg - other.deg;
		value = self.coef[-1] * Polynomial.invert(other.coef[-1])
		newCoef = [0]*(temp) + [value]
		return Polynomial(newCoef)

	def __divmod__(self, divisor):
		#~ print 'Divmod', self, '|', divisor
		if divisor.deg == -1:
			raise ArithmeticError('Dividing a Polynomial by zero.')

		d, m = [Polynomial([]), Polynomial(self.coef)]

		while m.deg != -1 and m.deg >= divisor.deg:
			temp = m._divideLeadTerms(divisor)
			#~ print d, '|', m, '|', temp, '|', divisor
			#~ print divisor, '|', m, '|', temp 
			#~ raw_input()
			d += temp
			m -= temp * divisor
		
		return d, m
	
	def __div__(self, other):
		return divmod(self, other)[0]

	def __mod__(self, other):
		return divmod(self, other)[1]
	
	def __str__(self):
		if len(self.coef) <= 0:
			return '0'
		
		ret = []
		if len(self.coef) > 0 and self.coef[0]:
			ret.append('%d' % self.coef[0])
		if len(self.coef) > 1 and self.coef[1]:
			if self.coef[1] == 1:
				ret.append('x')
			else:
				ret.append('%dx' % self.coef[1])
		for i in xrange(2, len(self.coef)):
			if self.coef[i] == 1:
				ret.append('x^%d' % i)
			elif self.coef[i]:
				ret.append('%dx^%d' % (self.coef[i], i))
		
		return ' + '.join(ret)
	
	__repr__ = __str__
	
	
	@staticmethod
	def invert(n):
		for a in xrange(1, mod):
			if (a * n) % mod == 1:
				return a
		raise ArithmeticError('No inverse for %d mod %d' % (n, mod))
	
	@staticmethod
	def gcd(a, b):
		#~ print 'Gcd'
		if b.deg == -1:
			return a
		else:
			return Polynomial.gcd(b, a%b)
	
	def __eq__(self, other):
		if isinstance(other, Polynomial):
			return self.coef == other.coef
		else:
			return self.deg == 0 and self.coef[0] == other
		return False

	def __ne__(self, other):
		return not (self == other)

	@staticmethod
	def findRandomPrimitive(prime, power):
		n = prime**power
		temp = [0]*(n+1)
		temp[-1] = 1
		temp[1] = prime-1
		cur = Polynomial.randomPolynomial(prime, power)
		while Polynomial.isReducible(cur, prime) or not Polynomial.isPrimitive(cur, n):
			#~ print 'Trying', cur
			cur = Polynomial.randomPolynomial(prime, power)
		return cur

	@staticmethod
	def isPrimitive(p, n):
		#~ print 'Checking primitivity'
		d = n-1
		if p == Polynomial.monomial(1):
			return False

		for x in xrange(1, d):
			if d % x == 0 and Polynomial.monomial(x)%p == 1:
				return False
		return True
	
	@staticmethod
	def isReducible(p, m):
		#~ print 'Checking reducibility'
		for i in xrange(1, p.deg):
			coef = [0]*(1 + m**i)
			coef[-1] = 1
			coef[1] = m-1
			test = Polynomial(coef)
			if(Polynomial.gcd(p, test).deg > 0):
				return True
		return False
	
	@staticmethod
	def randomPolynomial(m, deg):
		coef = [0]*(deg+1)
		coef[-1] = 1;
		for i in xrange(len(coef)-1):
			coef[i] = randrange(m)
		return Polynomial(coef)

def zeropad(arr, L):
	ret = tuple(arr)
	return ret + (0,)*(L-len(ret))

def setModulus(prime):
	global mod
	mod = prime

def GF(P, R, prim=None, verbose=False):
	Q = P**R
	if prim is None:
		prim = Polynomial.findRandomPrimitive(P, R)

	x = Polynomial([0,1])
	y = Polynomial([1])

	table = [(0,)*R]
	inv = {(0,)*R:0}

	if verbose:
		print 'Primitive:', prim
	i = 1
	while i==1 or y!=1:
		if i > Q:
			raise Exception('Not primitive! %s' % prim)
		table.append(zeropad(y.coef, R))
		inv[zeropad(y.coef, R)] = i
		
		if verbose:
			#~ print 'Number %d: %s' % (i, y)
			print 'Number %d: %s' % (i, zeropad(y.coef, R))
		i += 1
		y = (y*x) % prim

	return prim, table, inv

def inc(n, m):
	if n:
		return (n+1)%m or 1
	else:
		return 0

def increment(t):
	global mod
	i = 0
	while i < len(t)-1 and t[i]+1 >= mod:
		t[i] = 0
		i += 1
	
	if i >= len(t)-1:
		return None

	t[i] += 1
	#~ print t
	return t

def allPrims(P, R):
	n = P**R
	t = [0]*R + [1]
	while t is not None:
		cur = Polynomial(t)
		if not Polynomial.isReducible(cur, P) and Polynomial.isPrimitive(cur, n):
			yield cur
		t = increment(t)

'''
The GaloisField class is meant to alleviate my headache about
table lookups for elements of Galois Fields.

Elements can be operated on as GaloisElement objects. Anything valid
can be converted to a GaloisElement by using an instance of this
class as a callable. A GaloisElement can be turned into an int by
calling the int() function on it like anything else.

For example, this code prints all of PGL(2, 2^5):

f = GaloisField(2, 5)
for a in xrange(f.Q):
	for b in xrange(f.Q):
		for c in xrange(f.Q):
			for d in xrange(f.Q):
				#enforce that ad != bc
				if f(a)*f(d) == f(b)*f(c):
					continue
			
				#print for all x in the field
				for x in xrange(f.Q):
					if x != i:
						print (f(a)*f(x) + f(b)) / (f(c)*f(x) + f(d)),
					else:
						print f.Q,
				
				#print for x is infinity
				if c == 0:
					print f.Q
				else:
					print f(a) / f(c)

And this code prints all of AGL(31, 2):
f = GaloisField(31, 2)
for a in xrange(1, f.Q):
	for b in xrange(f.Q):
		#print for all x in the field
		for x in xrange(f.Q):
			print f(a)*f(x) + f(b),
		
		#print newline
		print

'''
class GaloisField:
	def __init__(self, P, R=None, prim=None, *args):
		if R is None:
			d = factorize(P)
			if len(d) != 1:
				raise ValueError('Cannot create galois field for %d' % P)
			
			P = d.keys()[0]
			R = d[P]

		self.P, self.R, self.prim = P, R, prim

		self.L = self.R>>1
		self.Q = P**R
		setModulus(P)
		
		if isinstance(prim, list):
			prim = Polynomial(prim)
		
		self.prim, self.table, self.inv = GF(P, R, prim)
		self.elements = [GaloisElement(self, x, self.table[x]) for x in xrange(self.Q)]
		
		self.tadd = None
		self.tsub = None
		self.tmul = None

	def calctadd(self):
		self.tadd = []
		for r in xrange(self.Q):
			row = []
			for c in xrange(self.Q):
				row.append(self.inv[zeropad((Polynomial(self.table[r]) + Polynomial(self.table[c])).coef, self.R)])
			self.tadd.append(row)

	def calctsub(self):
		self.tsub = []
		for r in xrange(self.Q):
			row = []
			for c in xrange(self.Q):
				row.append(self.inv[zeropad((Polynomial(self.table[r]) - Polynomial(self.table[c])).coef, self.R)])
			self.tsub.append(row)

	def calctmul(self):
		self.tmul = []
		for r in xrange(self.Q):
			row = []
			for c in xrange(self.Q):
				row.append(self.inv[zeropad((Polynomial(self.table[r]) * Polynomial(self.table[c]) % self.prim).coef, self.R)])
			self.tmul.append(row)

	@staticmethod
	def allFields(P, R=None):
		d = factorize(P)
		if len(d) != 1:
			raise ValueError('Cannot create galois field for %d' % P)
		
		P = d.keys()[0]
		R = d[P]

		setModulus(P)
		for prim in allPrims(P, R):
			yield GaloisField(P, R, prim)

	@staticmethod
	def randomPrim(P, R):
		setModulus(P)
		return Polynomial.findRandomPrimitive(P, R)

	def __call__(self, e):
		if isinstance(e, int):
			return self.elements[e]
		elif isinstance(e, list) or isinstance(e, tuple):
			return self.elements[self.inv[zeropad(e, self.R)]]
		elif isinstance(e, GaloisElement):
			return e
		else:
			raise ValueError('GaloisField __call__ argument cannot be of type "%s"' % type(e))

	def tinv(self, e):
		if e == 0:
			return -1
		elif e == 1:
			return 1
		else:
			return self.Q - e + 1

	def __eq__(self, other):
		return self.prim == other.prim and self.Q == other.Q

	def __ne__(self, other):
		return not (self == other)

	def __str__(self):
		return 'GF(%d ^ %d) (using %s)' % (self.P, self.R, self.prim)
	
	__repr__ = __str__

class GaloisElement:
	def __init__(self, field, index, coef):
		self.field = field
		self.index = index
		self.coef = coef
	
	def __add__(self, other):
		if self.field.tadd is None:
			self.field.calctadd()
		return self.field.elements[ self.field.tadd[self.index][other.index] ]

	def __sub__(self, other):
		if self.field.tsub is None:
			self.field.calctsub()
		return self.field.elements[ self.field.tsub[self.index][other.index] ]

	def __mul__(self, other):
		if self.field.tmul is None:
			self.field.calctmul()
		return self.field.elements[ self.field.tmul[self.index][other.index] ]

	def __div__(self, other):
		if other.index == 0:
			raise ArithmeticError('Galois Field division by zero')
		if self.field.tmul is None:
			self.field.calctmul()
		return self.field.elements[ self.field.tmul[self.index][other.inv().index] ]

	def __pow__(self, exp):
		if exp < 0:
			raise ArithmeticError('Cannot raise GF element to negative power')
		elif exp == 0:
			return self.field(1)
		elif exp == 1:
			return self
		elif exp&1:
			return self * pow(self, exp-1)
		else:
			pot = pow(self, exp>>1)
			return pot*pot

	def inv(self):
		return self.field.elements[ self.field.tinv(self.index) ]

	def suf(self):
		ret = 0
		i = self.field.R - 1
		while self.field.L+1 <= i:
			ret *= self.field.P
			ret += self.coef[i]
			i -= 1
		return ret
	
	def pre(self):
		return sum(self.coef[:self.field.L+1]) % self.field.P

	def __eq__(self, other):
		if isinstance(other, int):
			return self.index == other
		
		return (self.field == other.field
			and self.index == other.index
			and self.coef == other.coef)

	def __ne__(self, other):
		return not (self == other)

	def __str__(self):
		return str(self.index)
	
	def __repr__(self):
		return 'GF element %d with coef %s' % (self.index, self.coef)

	def __int__(self):
		return self.index

	# yeah, don't mix GaloisElements of different fields...
	def __hash__(self):
		return hash(self.index)