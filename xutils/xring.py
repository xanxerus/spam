#!/usr/bin/pypy
# !/usr/bin/pypy -i

from random import randrange
from xmath import factorize

class StablePolynomial:
    def __init__(self, coef, mod):
        i = len(coef) - 1
        while i >= 0 and coef[i] == 0:
            i -= 1

        self.deg = i
        self.coef = coef[:i + 1]
        self.mod = mod

    @staticmethod
    def zero(mod):
        return StablePolynomial([], mod)

    @staticmethod
    def monomial(n, mod):
        return StablePolynomial([0] * n + [1], mod)

    def __mul__(self, other):
        if self.mod != other.mod:
            raise ValueError('Cannot multiply polynomials with differing modulus')

        if self.deg == -1 or other.deg == -1:
            return StablePolynomial([], self.mod)

        newCoef = [0] * (self.deg + other.deg + 1)
        for i in xrange(len(self.coef)):
            for j in xrange(len(other.coef)):
                newCoef[i + j] = (self.coef[i] * other.coef[j] + newCoef[i + j]) % self.mod
        return StablePolynomial(newCoef, self.mod)

    def __add__(self, other):
        if self.mod != other.mod:
            raise ValueError('Cannot add polynomials with differing modulus')

        newCoef = [0] * max(len(self.coef), len(other.coef))
        for i in xrange(len(newCoef)):
            if i < len(self.coef):
                newCoef[i] = (newCoef[i] + self.coef[i]) % self.mod
            if i < len(other.coef):
                newCoef[i] = (newCoef[i] + other.coef[i]) % self.mod

        return StablePolynomial(newCoef, self.mod)

    def __sub__(self, other):
        if self.mod != other.mod:
            raise ValueError('Cannot subtract polynomials with differing modulus')

        newCoef = [0] * max(len(self.coef), len(other.coef))
        for i in xrange(len(newCoef)):
            if i < len(self.coef):
                newCoef[i] = (newCoef[i] + self.coef[i]) % self.mod
            if i < len(other.coef):
                newCoef[i] = (newCoef[i] - other.coef[i]) % self.mod

        return StablePolynomial(newCoef, self.mod)

    # no idea what this does
    def _divideLeadTerms(self, other):
        if self.mod != other.mod:
            raise ValueError('Cannot divideLeadTerms of polynomials with differing modulus')

        temp = self.deg - other.deg;
        value = self.coef[-1] * StablePolynomial.invert(other.coef[-1], self.mod)
        newCoef = [0] * (temp) + [value]
        return StablePolynomial(newCoef, self.mod)

    def __divmod__(self, divisor):
        if self.mod != divisor.mod:
            raise ValueError('Cannot divide polynomials with differing modulus')

        if divisor.deg == -1:
            raise ArithmeticError('Dividing a Polynomial by zero.')

        d, m = [StablePolynomial([], self.mod), StablePolynomial(self.coef, self.mod)]

        while m.deg != -1 and m.deg >= divisor.deg:
            temp = m._divideLeadTerms(divisor)
            d += temp
            m -= temp * divisor

        return d, m

    def __div__(self, other):
        return divmod(self, other)[0]

    def __mod__(self, other):
        return divmod(self, other)[1]

    def __str__(self):
        if len(self.coef) <= 0:
            return '0'

        ret = []
        if len(self.coef) > 0 and self.coef[0]:
            ret.append('%d' % self.coef[0])
        if len(self.coef) > 1 and self.coef[1]:
            if self.coef[1] == 1:
                ret.append('x')
            else:
                ret.append('%dx' % self.coef[1])
        for i in xrange(2, len(self.coef)):
            if self.coef[i] == 1:
                ret.append('x^%x' % i)
            elif self.coef[i]:
                ret.append('%dx^%x' % (self.coef[i], i))

        return ' + '.join(ret)

    __repr__ = __str__

    @staticmethod
    def invert(n, mod):
        for a in xrange(1, mod):
            if (a * n) % mod == 1:
                return a
        raise ArithmeticError('No inverse for %d mod %d' % (n, mod))

    @staticmethod
    def gcd(a, b):
        if b.deg == -1:
            return a
        else:
            return StablePolynomial.gcd(b, a % b)

    def __eq__(self, other):
        if isinstance(other, StablePolynomial):
            return self.mod == other.mod and self.coef == other.coef
        else:
            return len(self.coef) == 1 and self.coef[0] == other

    def __ne__(self, other):
        return not (self == other)

    @staticmethod
    def findRandomPrimitive(prime, power):
        n = prime ** power
        temp = [0] * (n + 1)
        temp[-1] = 1
        temp[1] = prime - 1
        cur = StablePolynomial.randomPolynomial(prime, power)
        while StablePolynomial.isReducible(cur, prime) or not StablePolynomial.isPrimitive(cur, n, prime):
            cur = StablePolynomial.randomPolynomial(prime, power)
        return cur

    @staticmethod
    def isPrimitive(p, n, mod):
        # ~ print 'Checking primitivity'
        d = n - 1
        if p == StablePolynomial.monomial(1, mod):
            return False

        for x in xrange(1, d):
            if d % x == 0 and StablePolynomial.monomial(x, mod) % p == 1:
                return False
        return True

    @staticmethod
    def isReducible(p, m):
        # ~ print 'Checking reducibility'
        for i in xrange(1, p.deg):
            coef = [0] * (1 + m ** i)
            coef[-1] = 1
            coef[1] = m - 1
            test = StablePolynomial(coef, m)
            if (StablePolynomial.gcd(p, test).deg > 0):
                return True
        return False

    @staticmethod
    def randomPolynomial(m, deg):
        coef = [0] * (deg + 1)
        coef[-1] = 1;
        for i in xrange(len(coef) - 1):
            coef[i] = randrange(m)
        return StablePolynomial(coef, m)


def zeropad(arr, L):
    ret = tuple(arr)
    return ret + (0,) * (L - len(ret))


def inc(n, m):
    if n:
        return (n + 1) % m or 1
    else:
        return 0


def increment(t, mod):
    i = 0
    while i < len(t) - 1 and t[i] + 1 >= mod:
        t[i] = 0
        i += 1

    if i >= len(t) - 1:
        return None

    t[i] += 1
    # ~ print t
    return t


def allPrims(P, R):
    n = P ** R
    t = [0] * R + [1]
    while t is not None:
        cur = StablePolynomial(t, P)
        if not StablePolynomial.isReducible(cur, P) and StablePolynomial.isPrimitive(cur, n, P):
            yield cur
        t = increment(t)


class StableField:
    @staticmethod
    def generate_gf_tables(P, R, prim=None, verbose=False):
        Q = P ** R
        if prim is None:
            prim = StablePolynomial.findRandomPrimitive(P, R)

        x = StablePolynomial([0, 1], P)
        y = StablePolynomial([1], P)

        table = [(0,) * R]
        inv = {(0,) * R: 0}

        # print Q, P, R, prim
        if verbose:
            print 'Primitive:', prim
        i = 1
        while i == 1 or y != 1:
            if i > Q:
                raise Exception('Not primitive! %s' % prim)
            table.append(zeropad(y.coef, R))
            inv[zeropad(y.coef, R)] = i

            if verbose:
                print 'Number %d: %s' % (i, zeropad(y.coef, R))
            i += 1
            y = (y * x) % prim

        return prim, table, inv

    def __init__(self, P, R=None, prim=None, *args):
        if R is None:
            d = factorize(P)
            if len(d) != 1:
                raise ValueError('Cannot create galois field for %d' % P)

            P = d.keys()[0]
            R = d[P]

        self.P, self.R, self.prim = P, R, prim
        self.L = self.R >> 1
        self.Q = P ** R

        if isinstance(prim, list):
            prim = StablePolynomial(prim, P)

        self.prim, self.table, self.inv = StableField.generate_gf_tables(P, R, prim)
        self.elements = [StableFieldElement(self, x, self.table[x]) for x in xrange(self.Q)]

        self.tadd = None
        self.tsub = None
        self.tmul = None

    def calctadd(self):
        self.tadd = []
        for r in xrange(self.Q):
            row = []
            for c in xrange(self.Q):
                row.append(self.inv[zeropad((StablePolynomial(self.table[r], self.P) + StablePolynomial(self.table[c], self.P)).coef, self.R)])
            self.tadd.append(row)

    def calctsub(self):
        self.tsub = []
        for r in xrange(self.Q):
            row = []
            for c in xrange(self.Q):
                row.append(self.inv[zeropad((StablePolynomial(self.table[r], self.P) - StablePolynomial(self.table[c], self.P)).coef, self.R)])
            self.tsub.append(row)

    def calctmul(self):
        # print self.table
        self.tmul = []
        for r in xrange(self.Q):
            row = []
            for c in xrange(self.Q):
                row.append(self.inv[zeropad((StablePolynomial(self.table[r], self.P) * StablePolynomial(self.table[c], self.P) % self.prim).coef, self.R)])
            self.tmul.append(row)

    @staticmethod
    def allFields(P, R=None):
        d = factorize(P)
        if len(d) != 1:
            raise ValueError('Cannot create galois field for %d' % P)

        P = d.keys()[0]
        R = d[P]

        for prim in allPrims(P, R):
            yield StableField(P, R, prim)

    @staticmethod
    def randomPrim(P, R):
        return StablePolynomial.findRandomPrimitive(P, R)

    def __call__(self, e):
        if isinstance(e, int):
            return self.elements[e]
        elif isinstance(e, list):
            return self.elements[self.inv[zeropad(e, self.R)]]
        elif isinstance(e, StableFieldElement):
            return e
        else:
            raise ValueError('StableField __call__ argument cannot be of type "%s"' % type(e))

    def tinv(self, e):
        if e == 0:
            return -1
        elif e == 1:
            return 1
        else:
            return self.Q - e + 1

    def __eq__(self, other):
        return self.prim == other.prim and self.Q == other.Q

    def __ne__(self, other):
        return not (self == other)

    def __str__(self):
        return 'StableField(%d ^ %d) (using %s)' % (self.P, self.R, self.prim)

    __repr__ = __str__


class StableFieldElement:
    def __init__(self, field, index, coef):
        self.field = field
        self.index = index
        self.coef = coef

    def __add__(self, other):
        if self.field.tadd is None:
            self.field.calctadd()
        return self.field.elements[self.field.tadd[self.index][other.index]]

    def __sub__(self, other):
        if self.field.tsub is None:
            self.field.calctsub()
        return self.field.elements[self.field.tsub[self.index][other.index]]

    def __mul__(self, other):
        if self.field.tmul is None:
            self.field.calctmul()
        return self.field.elements[self.field.tmul[self.index][other.index]]

    def __div__(self, other):
        if other.index == 0:
            raise ArithmeticError('StableFieldElement cannot divide by zero')
        if self.field.tmul is None:
            self.field.calctmul()
        return self.field.elements[self.field.tmul[self.index][other.inv().index]]

    def __pow__(self, exp):
        if exp < 0:
            raise ArithmeticError('StableFieldElement cannot raise to negative power')
        elif exp == 0:
            return self.field(1)
        elif exp == 1:
            return self
        elif exp & 1:
            return self * pow(self, exp - 1)
        else:
            pot = pow(self, exp >> 1)
            return pot * pot

    def inv(self):
        return self.field.elements[self.field.tinv(self.index)]

    def suf(self):
        ret = 0
        i = self.field.R - 1
        while self.field.L + 1 <= i:
            ret *= self.field.P
            ret += self.coef[i]
            i -= 1
        return ret

    def pre(self):
        return sum(self.coef[:self.field.L + 1]) % self.field.P

    def __eq__(self, other):
        if isinstance(other, int):
            return self.index == other

        return (self.field == other.field
                and self.index == other.index
                and self.coef == other.coef)

    def __ne__(self, other):
        return not (self == other)

    def __str__(self):
        return str(self.index)

    def __repr__(self):
        return 'StableFieldElement %d with coef %s' % (self.index, self.coef)

    def __int__(self):
        return self.index

class Ring:
    def __init__(self, N):
        self.N = N
        self.fields = []

        facs = factorize(N)
        parr = sorted(facs.keys())
        for k in parr:
            self.fields.append(StableField(k, facs[k]))

        # self.partials = []
        # last = 1
        # for k in reversed(parr):
        #     self.partials = [last] + self.partials
        #     last *= k**facs[k]
        #
        R = [f(0) for f in self.fields]
        self.table = [(0,)*len(self.fields)]
        self.inv = {(0,)*len(self.fields): 0}
        self.elements = [RingElement(self, 0, R)]

        I = [f(1) for f in self.fields]
        for x in xrange(1, N):
            R = [r+i for r,i in zip(R, I)]
            t_R = tuple(map(int, R))
            self.table.append(t_R)
            self.inv[t_R] = x
            self.elements.append(RingElement(self, x, R))

        self.tadd = None
        self.tsub = None
        self.tmul = None

    def calctadd(self):
        self.tadd = []
        for r in xrange(self.N):
            row = []
            for c in xrange(self.N):
                q = tuple(int(a+b) for a, b in zip(self.elements[r].coef, self.elements[c].coef))
                row.append(self.inv[q])
            self.tadd.append(row)

    def calctsub(self):
        self.tsub = []
        for r in xrange(self.N):
            row = []
            for c in xrange(self.N):
                q = tuple(int(a-b) for a, b in zip(self.elements[r].coef, self.elements[c].coef))
                row.append(self.inv[q])
            self.tsub.append(row)

    def calctmul(self):
        self.tmul = []
        for r in xrange(self.N):
            row = []
            for c in xrange(self.N):
                q = tuple(int(a*b) for a, b in zip(self.elements[r].coef, self.elements[c].coef))
                row.append(self.inv[q])
            self.tmul.append(row)

    def __call__(self, e):
        if isinstance(e, int):
            return self.elements[e]
        elif isinstance(e, list) or isinstance(e, tuple):
            return self.elements[self.inv[tuple(e)]]
        elif isinstance(e, RingElement):
            return e
        else:
            raise ValueError('Ring __call__ argument cannot be of type "%s"' % type(e))

    def __str__(self):
        return 'Ring on %d with fields on <%s>' % (self.N, ', '.join(str(f.Q) for f in self.fields))

    def __eq__(self, other):
        return self.N == other.N and self.fields == other.fields

    def __ne__(self, other):
        return not (self == other)

    __repr__ = __str__

class RingElement:
    def __init__(self, ring, index, coef):
        self.ring = ring
        self.index = index
        self.coef = coef

    def __add__(self, other):
        if self.ring.tadd is None:
            self.ring.calctadd()
        return self.ring.elements[self.ring.tadd[self.index][other.index]]

    def __sub__(self, other):
        if self.ring.tsub is None:
            self.ring.calctsub()
        return self.ring.elements[self.ring.tsub[self.index][other.index]]

    def __mul__(self, other):
        if self.ring.tmul is None:
            self.ring.calctmul()
        return self.ring.elements[self.ring.tmul[self.index][other.index]]

    def __div__(self, other):
        raise ValueError("Cannot divide RingElement")
        # if other.index == 0:
        #     raise ArithmeticError('RingElement cannot divide by zero')
        # if self.ring.tmul is None:
        #     self.ring.calctmul()
        # return self.ring.elements[self.ring.tmul[self.index][other.inv().index]]

    def __pow__(self, exp):
        if exp < 0:
            raise ArithmeticError('RingElement cannot raise to negative power')
        elif exp == 0:
            return self.ring(1)
        elif exp == 1:
            return self
        elif exp & 1:
            return self * pow(self, exp - 1)
        else:
            pot = pow(self, exp >> 1)
            return pot * pot

    # def inv(self):
    #     return self.ring.elements[self.ring.tinv(self.index)]

    # def suf(self):
    #     ret = 0
    #     i = self.ring.R - 1
    #     while self.ring.L + 1 <= i:
    #         ret *= self.ring.P
    #         ret += self.coef[i]
    #         i -= 1
    #     return ret
    #
    # def pre(self):
    #     return sum(self.coef[:self.ring.L + 1]) % self.ring.P

    def __eq__(self, other):
        if isinstance(other, int):
            return self.index == other

        return (self.ring == other.ring
                and self.index == other.index
                and self.coef == other.coef)

    def __ne__(self, other):
        return not (self == other)

    def __str__(self):
        return str(self.index)

    def __repr__(self):
        return 'RingElement %d <%s>' % (self.index, ', '.join(map(str, self.coef)))

    def __int__(self):
        return self.index

