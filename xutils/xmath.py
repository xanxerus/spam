#!/usr/bin/pypy -i

from math import sqrt
from fractions import gcd, Fraction as F

def era(n):
	arr = [0,0] + range(2, n+1)
	for x in arr:
		if x:
			for y in xrange(x<<1, n+1, x):
				arr[y] = 0
	return filter(None, arr)

def befactorize(n):
	arr = [{}, {}] + range(2, n+1)
	for x in xrange(2, n+1):
		if isinstance(arr[x], int):
			arr[x] = {x:1}
			for y in xrange(x<<1, n+1, x):
				r = 0
				q = y
				while q%x==0:
					q/=x
					r+=1
				if isinstance(arr[y], int):
					arr[y] = {x:r}
				else:
					arr[y][x] = r
	return arr

def awesometotient(n):
	arr = range(n+1)
	for x in xrange(2, n+1):
		if isinstance(arr[x], int):
			arr[x] = x-1
			for y in xrange(x<<1, n+1, x):
				if isinstance(arr[y], int):
					arr[y] = {x}
				else:
					arr[y].add(x)
		else:
			t = F(x)
			for p in arr[x]:
				t *= F(p-1, p)
			arr[x] = t.numerator
	return arr

def naivetotient(n, scarce=False):
	if n < 2:
		return 1
	
	t = F(n)
	for p in factorize(n, scarce):
		t *= F(p-1, p)
	return t.numerator


def isprime(n):
	if n < 4:
		return n > 1
	if n % 2 == 0 or n % 3 == 0:
		return False
	for x in xrange(5, 1 + int(sqrt(n)), 6):
		if n%x==0 or n%(x+2)==0:
			return False
	return True

def primegenerator(L):
	for x in xrange(2, L+1):
		if isprime(x):
			yield x

def factorize(n, scarce=False):
	ret = dict()

	for p in (primegenerator() if scarce else era(n)):
		r = 0
		while n%p == 0:
			r += 1
			n /= p
		if r:
			ret[p] = r
		
		if n == 1:
			return ret
