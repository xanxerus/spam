from xmath import isprime, factorize
import datetime, time

def timestamp():
	return datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H:%M:%S')

def compose(a, b):
	return [b[e] for e in a]

def invert(a):
	ret = [0]*(len(a))
	for i, e in enumerate(a):
		ret[e] = i
	return ret

def big_compose(perm, block):
	return [compose(perm, pi) for pi in block]

# Compute c + k, wrapping around n in the style of Primitive AGL
def gamma(c, k, n):
	if c == 0:
		return 0
	elif c < n-k:
		return c+k
	else:
		return c+k-n+1

# Compute c - k, wrapping around n in the style of Primitive AGL
def gammainverse(d, k, n):
	if d == 0:
		return 0
	elif d > k:
		return d-k
	else:
		return d-k+n-1

def pp(perm):
	if perm:
		return ' '.join(map(str, perm))
	else:
		return ''

def parsePR(PR):
	if '^' in PR:
		P, R = map(int, PR.split('^'))
		if not isprime(P):
			raise ValueError('%s is not a prime power' % PR)
	else:
		fac = factorize(int(PR))
		if len(fac) != 1:
			raise ValueError('%s is not a prime power' % PR)
		P = fac.keys()[0]
		R = fac[P]
		
		if not isprime(P):
			raise ValueError('%s is not a prime power' % PR)

	return P, R

def join_pa(pa):
	ret = []
	for coset in pa:
		ret.extend(coset)
	return ret

''' Given a filename and possibly a number of blocks, reads the file and
	returns the PA, divided into blocks of size blocksize (or one block if 0)
'''
def parse_pa(filename, blocksize=0, limit=None, join=False):
	pa = []
	mac = False
	with open(filename, 'U') as f:
		coset = []
		
		for line in f:
			if '#' in line:
				line = line[:line.index('#')]

			s = line.strip()
			if s:
				coset.append(map(int, s.split()))

			if (not join) and (not s or (blocksize and len(coset) == blocksize)):
				pa.append(coset)
				coset = []
				if limit and limit == len(pa):
					break

		if coset:
			pa.append(coset)
	
	#~ print map(len, pa)
	if join:
		return pa[0]
	else:
		return pa

def parse_system(filename):
	with open(filename, 'r') as f:
		lines = f.readlines()
		
		if len(lines) == 1 and '\r' in lines[0]:
			lines = lines[0].split('\r')
		
		#xander style
		lines = filter(lambda s: s and not s.startswith('#'), map(str.strip, lines))
		try:
			return json.loads(''.join(lines))
		except:
			pass
		
		#luis style
		return json.loads('[%s,%s]' % (lines[0][2:], lines[1][2:]))
