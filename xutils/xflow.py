#!/usr/bin/pypy -i
from collections import deque
from copy import deepcopy

'''s a,1 b,1 c,1
a y,1 z,1
b y,1
c y,1
x t,1
y t,1
z t,1
'''

def BFS(G, start='s', end='t'):
	for k in G:
		G[k]['color'] = 0
		G[k]['d'] = None
		G[k]['parent'] = None

	G['s']['color'] = 1
	G['s']['d'] = 0
	G['s']['parent'] = None
	
	Q = deque()
	Q.append('s')

	while Q:
		u = Q.popleft()
		for v in G[u]:
			if v in ('parent', 'color', 'd'):
				continue
			if G[v]['color'] == 0 and G[u][v]:
				G[v]['color'] = 1
				G[v]['d'] = G[u]['d'] + 1
				G[v]['parent'] = u
				Q.append(v)
		G[u]['color'] = 2

	#~ print 'Graph:'
	#~ for a in G:
		#~ print a, G[a]
	#~ print

def update(G):
	#~ print 'Path:'
	u = 't'
	while True:
		v = G[u]['parent']
		if v is None:
			break
		#~ print u, v
		G[v][u] -= 1
		G[u][v] += 1
		u = v
	#~ print 

''' Given sets A and B and a function of a in A which returns the
	set of vertices unto which A is incident, find the maximum
	bipartite matching and return the edges involved.
'''
def match(A, B, adj):
	G = dict()
	
	#everything in B can go to the sink
	G['t'] = dict()
	for b in B:
		G['t'][b] = 0
		G[b] = {'t':1}

	#the source can go to anything in A
	#Also A can go to its corresponding B values
	G['s'] = dict()
	for a in A:
		G['s'][a] = 1
		G[a] = {'s':0}
		for e in adj(a):
			G[a][e] = 1
			G[e][a] = 0

	#Edmonds Karp
	while True:
		BFS(G)
		if G['t']['parent'] is not None:
			update(G)
		else:
			break

	edges = list()
	for b in B:
		for a in G[b]:
			if a not in ('t', 'd', 'color', 'parent') and G[b][a]:
				edges.append((b, a))

	return edges


#~ G = parseGraph()
#~ Q = {'s': {(0, 0): 1, (0, 1): 1, (0, 2): 1}, (0, 0): {'s': 0, 8: 1, 9: 1}, (0, 1): {'s': 0, 8: 1}, (0, 2): {'s': 0, 8: 1}, 8: {(0, 0): 0, (0, 1): 0, (0, 2): 0, 't': 1}, 9: {(0, 0): 0, 't': 1}, 7: {'t': 1}, 't': {7: 0, 8: 0, 9: 0}}
#~ R = maxflow(Q)
