#!/usr/bin/pypy -i

from xfield import GaloisField
from xutils import gamma

#the disjoint set ADT
class DisjointSet:
	def __init__(self, n):
		self.n = n
		self.arr = [-1]*n

	def find(self, e):
		if self.arr[e] < 0:
			return e
		else:
			pot = self.find(self.arr[e])
			self.arr[e] = pot
			return pot

	def union(self, x, y):
		xRoot = self.find(x)
		yRoot = self.find(y)
		
		if xRoot == yRoot:
			return
		
		if -self.arr[xRoot] - 1 < -self.arr[yRoot] - 1:
			self.arr[xRoot] = yRoot
			return yRoot
		elif -self.arr[xRoot] - 1 > -self.arr[yRoot] - 1:
			self.arr[yRoot] = xRoot
			return xRoot
		else:
			self.arr[xRoot] = yRoot
			self.arr[yRoot] -= 1
			return yRoot

	def __str__(self):
		f = '%' + str(len(str(self.n)) + 1) + 'd'
		ret = 'K: ' + ' '.join(f % x for x in xrange(self.n)) + '\n'
		ret += 'V: ' + ' '.join(f % x for x in self.arr)
		return ret

	__repr__ = __str__

#un-extend a row
def contract(row, N=None):
	if N is None:
		N = len(row)-1
	
	ret = row[:-1]
	i = row.index(N)
	if i < len(ret):
		ret[i] = row[-1]
	return ret, i, row[-1]

def in_agl_k(f, perm, k):
	r = perm[0]
	for c in xrange(len(perm)):
		if perm[c] != int(f(r) + f(gamma(c, k, f.Q))):
			return False
	return True

def in_modagl_k(perm, k):
	N = len(perm)
	a = k+1
	b = perm[0]
	for x in xrange(len(perm)):
		if (a*x + b) % N != perm[x]:
			return False
	return True

#returns the GaloisField for a given zero-coset non-header permutation of AGL
def reconstructField(row):
	if row[0] == 0:
		raise ValueError('Cannot reconstruct from a row that starts with 0')

	N = len(row)
	for f in GaloisField.allFields(N):
		pot = []
		for i in xrange(N):
			pot.append(int(f(row[0]) + f(i)))
		if row == pot:
			return f
	return None

#returns the first row in a block that does not start with 0
#if e is given, all permutations will be contracted e times first
def firstNonheader(block, e=0):
	for perm in block:
		for _ in xrange(e):
			perm = contract(perm)[0]
		if perm[0] != 0:
			return perm

#turn a pa into its cosets, P, and Q 
def blockReversal(block, sequential=False):
	N = len(block[0])
	kitty = DisjointSet(2*N)

	for perm in block:
		c, p, q = contract(perm, N-1)
		kitty.union(p, q+N)

	pis = []
	pi = []
	remainingP = set(xrange(N))
	remainingQ = set(xrange(N))
	P, Q, E = [], [], []
	for perm in block:
		c, p, q = contract(perm, N-1)
		remainingP.discard(p)
		remainingQ.discard(q)
		e = kitty.find(p)
		
		if len(E) > 0 and E[-1] == e:
			P[-1].add(p)
			Q[-1].add(q)
			pi.append(c)
		else:
			P.append({p})
			Q.append({q})
			E.append(e)
			if pi:
				pis.append(pi)
				pi = []
			pi.append(c)

	if pi:
		pis.append(pi)

	P, Q, remainingP, remainingQ = map(sorted, P), map(sorted, Q), sorted(remainingP), sorted(remainingQ)

	if sequential:
		F = []
		i = 0
		while i < len(P):
			if len(P[i]) == 1 and P[i][0] == N-2:
				F[-1] = True
				del P[i]
				del Q[i]
			else:
				F.append(False)
				i += 1
		return pis, P, Q, remainingP, remainingQ, F
	
	else:
		hasFreebie = len(P[-1]) == len(Q[-1]) == 1 and P[-1][0] == Q[-1][0] == N-1
		
		if hasFreebie:
			P.pop()
			Q.pop()

		return pis, P, Q, remainingP, remainingQ, hasFreebie

#turns a sequential PA into its cosets, Ps, and Qs
def sequentialReversal(block):
	pis, P2, Q2, xP2, xQ2, F2 = blockReversal(block)
	
	cosets, P1s, Q1s, xP1s, xQ1s, F1s = [], [], [], [], [], []
	
	for pi in pis:
		k, P, Q, xP, xQ, hasFreebie = blockReversal(pi)
		cosets.extend(k)
		P1s.append(P)
		Q1s.append(Q)
		xP1s.append(xP)
		xQ1s.append(xQ)
		F1s.append(hasFreebie)

	return cosets, P2, Q2, xP2, xQ2, P1s, Q1s, xP1s, xQ1s, F2, F1s

#turn a block into its field, P, and Q
#assuming the block is one level P&E on group AGL
def AGLblockReversal(block, f=None, k=0):
	N = len(block[0])-1

	if f is None:
		f = reconstructField(firstNonheader(block, 1))

	P = []
	Q = []
	remainingP = set()
	remainingQ = set()

	pi = []
	P_k = set()
	Q_k = set()

	for i, perm in enumerate(block):
		aglperm, p, q = contract(perm, N)
		
		#if the coset ended, move on
		while not in_agl_k(f, aglperm, k):
			P.append(sorted(P_k))
			Q.append(sorted(Q_k))
			remainingP -= P_k
			remainingQ -= Q_k
			P_k = set()
			Q_k = set()
			k += 1
			
			if k >= N-1:
				raise ValueError('Permutation %d is not in AGL' % i)
		
		#now it's the same coset
		P_k.add(p)
		Q_k.add(q)

	if P_k:
		P.append(sorted(P_k))
		Q.append(sorted(Q_k))
		remainingP -= P_k
		remainingQ -= Q_k
		P_k = []
		Q_k = []

	remainingP, remainingQ = sorted(remainingP), sorted(remainingQ)

	hasFreebie = len(P[-1]) == len(Q[-1]) == 1 and P[-1][0] == Q[-1][0] == N
	
	if hasFreebie:
		P.pop()
		Q.pop()

	return f, P, Q, remainingP, remainingQ, hasFreebie

#turn a block into its field, P1, Q1, P2, Q2
#assuming the block is from sequential P&E on group AGL
def AGLsequentialReversal(block, f=None):
	N = len(block[0])-2

	if f is None:
		f = reconstructField(firstNonheader(block, 2))

	if f is None:
		raise ValueError('Error -46. Are you sure this was sequential from AGL?')

	#return values
	P2	= []
	Q2	= []
	P1	= []
	Q1	= []

	#state values
	k = 0
	P1_s = []
	Q1_s = []
	P_s = set()
	P_k = set()
	Q_s = set()
	Q_k = set()
	last_lv1_freebie = False
	last_lv2_freebie = False

	for i, perm in enumerate(block):
		lv2perm, p2, q2 = contract(perm)
		aglperm, p1, q1 = contract(lv2perm)
		
		if in_agl_k(f, aglperm, k): #same coset, same pi
			if p1 == N:
				last_lv1_freebie = True
			else:
				last_lv1_freebie = False
				P_k.add(p1)
				Q_k.add(q1)

			if p2 != N+1:
				P_s.add(p2)
				Q_s.add(q2)
		else:
			k += 1
			if not in_agl_k(f, aglperm, k):
				raise ValueError('Error -45. Send Xander the PA.')

			if not last_lv1_freebie: #new coset, same pi
				P1_s.append(sorted(P_k))
				Q1_s.append(sorted(Q_k))
				P_k, Q_k = set(), set()
			else: #new coset, new pi
				P1.append(P1_s)
				Q1.append(Q1_s)
				P1_s, Q1_s = [], []
				if p2 == N+1:
					last_lv2_freebie = True
				else:
					last_lv2_freebie = False
					P2.append(sorted(P_s))
					Q2.append(sorted(Q_s))
					P_s, Q_s = set(), set()

	#end coset, end freebie pi
	P1.append(P1_s)
	Q1.append(Q1_s)
	P2.append(sorted(P_s))
	Q2.append(sorted(Q_s))
	
	#~ print P1_s
	#~ print Q1_s
	#~ print P_s
	#~ print Q_s
	#~ print P_k
	#~ print Q_k

	return f, P2, Q2, P1, Q1

#turn a block into its field, P, and Q
#assuming the block is one level P&E on modular AGL
def modAGLblockReversal(block, k=0):
	N = len(block[0])-1
	P = []
	Q = []
	remainingP = set()
	remainingQ = set()

	pi = []
	P_k = set()
	Q_k = set()

	for i, perm in enumerate(block):
		aglperm, p, q = contract(perm, N)
		
		#if the coset ended, move on
		while not in_modagl_k(aglperm, k):
			P.append(sorted(P_k))
			Q.append(sorted(Q_k))
			remainingP -= P_k
			remainingQ -= Q_k
			P_k = set()
			Q_k = set()
			k += 1
			
			if k >= N-1:
				raise ValueError('Permutation %d is not in AGL' % i)
		
		#now it's the same coset
		P_k.add(p)
		Q_k.add(q)

	if P_k:
		P.append(sorted(P_k))
		Q.append(sorted(Q_k))
		remainingP -= P_k
		remainingQ -= Q_k
		P_k = []
		Q_k = []

	remainingP, remainingQ = sorted(remainingP), sorted(remainingQ)

	hasFreebie = len(P[-1]) == len(Q[-1]) == 1 and P[-1][0] == Q[-1][0] == N
	
	if hasFreebie:
		P.pop()
		Q.pop()

	return N, P, Q, remainingP, remainingQ, hasFreebie


#turn a block into its field, P1, Q1, P2, Q2
#assuming the block is from sequential P&E on modular AGL
def modAGLsequentialReversal(block):
	N = len(block[0])-2

	#return values
	P2	= []
	Q2	= []
	P1	= []
	Q1	= []

	#state values
	k = 0
	P1_s = []
	Q1_s = []
	P_s = set()
	P_k = set()
	Q_s = set()
	Q_k = set()
	last_lv1_freebie = False
	last_lv2_freebie = False

	for i, perm in enumerate(block):
		lv2perm, p2, q2 = contract(perm)
		aglperm, p1, q1 = contract(lv2perm)
		
		if in_modagl_k(aglperm, k): #same coset, same pi
			if p1 == N:
				last_lv1_freebie = True
			else:
				last_lv1_freebie = False
				P_k.add(p1)
				Q_k.add(q1)

			if p2 != N+1:
				P_s.add(p2)
				Q_s.add(q2)
		else:
			k += 1
			if not in_modagl_k(aglperm, k):
				print i, k, aglperm
				raise ValueError('Error -45. Send Xander the PA.')

			if not last_lv1_freebie: #new coset, same pi
				P1_s.append(sorted(P_k))
				Q1_s.append(sorted(Q_k))
				P_k, Q_k = set(), set()
			else: #new coset, new pi
				P1.append(P1_s)
				Q1.append(Q1_s)
				P1_s, Q1_s = [], []
				if p2 == N+1:
					last_lv2_freebie = True
				else:
					last_lv2_freebie = False
					P2.append(sorted(P_s))
					Q2.append(sorted(Q_s))
					P_s, Q_s = set(), set()

	#end coset, end freebie pi
	P1.append(P1_s)
	Q1.append(Q1_s)
	P2.append(sorted(P_s))
	Q2.append(sorted(Q_s))
	
	#~ print P1_s
	#~ print Q1_s
	#~ print P_s
	#~ print Q_s
	#~ print P_k
	#~ print Q_k

	return N, P2, Q2, P1, Q1
