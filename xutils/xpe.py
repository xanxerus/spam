#!/usr/bin/env pypy

#This file is for various simple partition and extension tools

from copy import deepcopy

''' Given a coset, a position, and a set of symbols Q, divide the coset
	into two lists, one of all uncovered permutations, and one of all covered.
	Based on the algorithm from Luis
'''
def split_coverage(coset, p, Q):
	uncovered_perm, covered_perm = [], []
	for pi in coset:
		(covered_perm if pi[p] in Q else uncovered_perm).append(pi)
	return uncovered_perm,covered_perm

''' Given a PA and a set Q, greedily find a good P for that Q
	Based on the algorithm from Luis
'''
def good_P_given_Q(pa, Q):
	pa = deepcopy(pa)
	N, K = len(pa[0][0]), len(pa)
	P = [[x] for x in range(K)]								#choose a naive partitioning of symbols

	for p in range(K, N):									#for each position
		least_uncovered, most_coverage, max_coset = [], 0, 0
		for y in range(len(pa)):							#find the max covered coset in that position
			uncovered, covered = split_coverage(pa[y], p, Q[y])
			if len(covered) > most_coverage:
				least_uncovered, most_coverage, max_coset = uncovered, len(covered), y

		P[max_coset].append(p)								#give that position to that max covered coset
		pa[max_coset]=least_uncovered						#remove covered permutations

	return P

''' Returns K roughly equal sized sets partitioning Z_N 
'''
def split_Q(N, K):
	#~ print N, K
	ret = []
	d, m = divmod(N, K)
	i = 0
	for x in xrange(K):
		if x < m:
			ret.append(range(i, i+d+1))
			i += d+1
		else:
			ret.append(range(i,i+d))
			i += d
	return ret

''' Given a pa and a level of parallelism G, naively and greedily choose
	G pairs of (P,Q) that partition the pa.
'''
def naive_partition(pa, G=1):
	assert G > 0
	Qnaive = split_Q(len(pa[0][0]), len(pa))
	#~ print Qnaive
	Pnaive = good_P_given_Q(pa, Qnaive)
	return naive_partition_given_PQ(Pnaive, Qnaive, G)

''' Given a P and Q and level of parallelism G, naively create a
	set of G pairs of (P, Q) that partition a PA.
'''
def naive_partition_given_PQ(Pnaive, Qnaive, G=2):
	args = []
	for g in xrange(G):
		args.append(Pnaive[g:] + Pnaive[:g])
		args.append(Qnaive[g:] + Qnaive[:g])
	return args


''' Given a coset and a symbol, append the symbol to the end of every
	permutation in the coset. Symbol is by default the length of permutations
'''
def freebify_coset(coset, n=None):
	return [pi + [n or len(coset[0])] for pi in coset]

''' Given a PA and a level of parallelization G, append G symbols to the
	ends of the last G cosets, cyclically shifting so that no new agreements
	are made
'''
def freebify_pa(pa, G=1):
	N, K = len(pa[0]), len(pa)
	ret = deepcopy(pa)
	for i in xrange(K-G, K):					#add freebies
		for g in xrange(G):						#circular shift the freebies
			ret[i] = freebify_coset(ret[i], N + (g + i - (K-G)) % G)
	return ret

''' Performs partition and extension greedily given a coset, P, and Q
'''
def extend_coset(coset, P, Q):
	ret = []
	for pi in coset:						#for each permutation
		for p in P:							#for each position
			for q in Q:						#for each symbol
				if pi[p] == q:				#if a symbol matches at a position
					pot = pi[:]
					pot.append(q)
					pot[p] = len(pi)		#extend the permutation
					ret.append(pot)			#and add it to the extended set
					break					#then jump to the next permutation
			else:
				continue
			break
	return ret

''' Given a PA and however many P and Q sets, parallel extend the PA
	DOES NOT DO FREEBIES
'''
def extend_pa(pa, *args):
	#assert that every coset has a P and a Q
	#~ print 'q', args
	#~ print len(pa), len(args)
	assert args and len(args) % 2 == 0 and len(pa) == len(args[0])
	N, K, G = len(pa[0]), len(args[0]), len(args)/2
	ret = []
	for i in xrange(K):								#for every coset
		ret.append(deepcopy(pa[i]))
		for g in xrange(0, len(args), 2):			#for each new symbol
			ret[i] = extend_coset(ret[i], args[g][i], args[g+1][i])
	return ret

''' Given a PA, multiple PQ systems, and the limit and amount of parallelism,
	extend all PIs including freebies
'''
def extend_sequential_pa(pa, multisystem):
	extpa = []
	i = 0
	for system in multisystem:
		subpa1 = pa[i:i+len(system[0])]
		subpa2 = pa[i+len(system[0]):i+len(system[0])+len(system)/2]
		extpa.extend(extend_pa(subpa1, *system))
		extpa.extend(freebify_pa(subpa2, len(system)/2))
		i += len(system[0])+len(system)/2
	return extpa

''' Given a PA and PQ partitions, return the number of covered permutations
'''
def coverage_degree(pa, *args):
	return sum(map(len, extend_pa(pa, *args)))

def onlinepe(onlinepa, P, Q):
	#~ assert len(P) == len(Q)
	k = 0
	N = None
	Q = map(set, Q)
	for perm in onlinepa:
		if perm:
			if N is None:
				N = len(perm)
			
			if k < len(P):
				#~ print 'perm', perm
				#~ print 'P[k]:', P[k]
				#~ print 'Q[k]:', Q[k]
				for p in P[k]:
					if perm[p] in Q[k]:
						perm.append(perm[p])
						perm[p] = N
						yield perm
						break
			elif k==len(P):
				yield perm + [N]
			else:
				break
		else:
			k += 1
			yield None

def onlinepi(onlinepa, P1, Q1):
	s, k = 0, 0
	S, K = len(P1), len(P1[0])
	N = None
	Q1 = [map(set, Q) for Q in Q1]
	for perm in onlinepa:
		if perm:
			if N is None:
				N = len(perm)
			
			#first level
			if k < K:
				for p in P1[s][k]:
					if perm[p] in Q1[s][k]:
						perm.append(perm[p])
						perm[p] = N
						yield perm
						break
			elif k == K:
				yield perm + [N]
			else:
				raise ValueError('Error -42: Xander sucks. Contact him.')
		else:
			k += 1
			if k > K:
				yield None
				k = 0
				s += 1
				if s >= S:
					break

'''

#Naive:

def onlinesequential(onlinepa, P1, Q1, P2, Q2):
	s, k = 0, 0
	S, K = len(P1), len(P1[0])
	N = None
	Q1 = [map(set, Q) for Q in Q1]
	Q2 = map(set, Q2)
	for perm in onlinepa:
		if perm:
			if N is None:
				N = len(perm)
			
			fl = False
			#first level
			if k < K:
				#~ print len(P1), s, len(P1[s]), k
				for p in P1[s][k]:
					if perm[p] in Q1[s][k]:
						perm.append(perm[p])
						perm[p] = N
						fl = True
						break
			elif k == K:
				fl = True
				perm = perm + [N]
			else:
				raise ValueError('Error -42: Xander sucks. Contact him.')
			
			if not fl:
				continue
			else:
				fl = False

			#second level
			if s < S-1:
				for p in P2[s]:
					#~ print len(perm), p, len(Q2), s
					if perm[p] in Q2[s]:
						perm.append(perm[p])
						perm[p] = N+1
						fl = True
						break
			elif s == S-1:
				fl = True
				perm = perm + [N+1]
			else:
				raise ValueError('Error -43: Xander sucks. Contact him.')
			
			if fl:
				yield perm
		else:
			k += 1
			if k > K:
				yield None
				k = 0
				s += 1
				if s >= S:
					break

'''

#anti-clash optimized:
def onlinesequential(onlinepa, P1, Q1, P2, Q2):
	s, k = 0, 0 # s is the pi index, k is the coset index
	S, K = len(P1), len(P1[0])
	N = None
	Q1 = [map(set, Q) for Q in Q1]
	Q2 = map(set, Q2)
	for perm in onlinepa:
		# end of a coset
		if not perm:
			k += 1
			if k > K:
				yield None
				k = 0
				s += 1
				if s >= S:
					break
			continue

		# first permutation
		if N is None:
			N = len(perm)
		
		if k < K and s < S-1: #collision potential

			# first level covered positions
			fl = set()
			for p in P1[s][k]:
				if perm[p] in Q1[s][k]:
					fl.add(p)

			# second level covered positions			
			sl = set()
			for p in P2[s]:
				if p != N and perm[p] in Q2[s]:
					sl.add(p)
			
			# here be dragons
			if len(fl - sl) and sl: #fl is not a subset of sl
				p = (fl-sl).pop()
				perm.append(perm[p])
				perm[p] = N
				
				sl.discard(p)

				p = sl.pop()
				perm.append(perm[p])
				perm[p] = N+1
				yield perm
			elif len(sl) > 1 and fl: # |sl| > 1
				p = fl.pop()
				perm.append(perm[p])
				perm[p] = N
				
				sl.discard(p)

				p = sl.pop()
				perm.append(perm[p])
				perm[p] = N+1
				yield perm
			else: #clash is inevitable unless we can collude
				yeld = False
				
				#Lv1 can put a symbol in Q2 into a position in N
				if N in P2[s]: 
					for p in fl:
						if perm[p] in Q2[s]: #collusion type 1, feed q to N
							perm.append(N+1)
							perm.append(perm[p])
							perm[p] = N
							yield perm
							yeld = True
							break
				
				#Lv1 can put N into a position in P2
				if N in Q2[s] and not yeld: 
					for p in fl:
						if p in P2[s]: #collusion type 2, feed N to p
							perm.append(perm[p])
							perm.append(N)
							perm[p] = N+1
							yield perm
							break
				#~ if not yeld:
				if False: #debug code for error checking, in case I suck
					kl = False
					#first level
					if k < K:
						#~ print len(P1), s, len(P1[s]), k
						for p in P1[s][k]:
							if perm[p] in Q1[s][k]:
								print 'Lv1 used p, q = %d, %d' % (p, perm[p])
								perm.append(perm[p])
								perm[p] = N
								kl = True
								break
					elif k == K:
						kl = True
						perm = perm + [N]
					else:
						raise ValueError('Error -42: Xander sucks. Contact him.')
					
					if not kl:
						continue
					else:
						kl = False

					#second level
					if s < S-1:
						for p in P2[s]:
							#~ print len(perm), p, len(Q2), s
							if perm[p] in Q2[s]:
								print 'Lv2 used p, q = %d, %d' % (p, perm[p])
								perm.append(perm[p])
								perm[p] = N+1
								kl = True
								break
					elif s == S-1:
						kl = True
						perm = perm + [N+1]
					else:
						raise ValueError('Error -43: Xander sucks. Contact him.')
					
					if kl:
						print perm
						print fl
						print sl
						print P1[s][k]
						print Q1[s][k]
						print P2[s]
						print Q2[s]
						raw_input()
						exit(1)


		else: #no clash potential, something is a freebie
			fl = False
			#first level
			if k < K:
				#~ print len(P1), s, len(P1[s]), k
				for p in P1[s][k]:
					if perm[p] in Q1[s][k]:
						perm.append(perm[p])
						perm[p] = N
						fl = True
						break
			elif k == K:
				fl = True
				perm = perm + [N]
			else:
				raise ValueError('Error -42: Xander sucks. Contact him.')
			
			if not fl:
				continue
			else:
				fl = False

			#second level
			if s < S-1:
				for p in P2[s]:
					#~ print len(perm), p, len(Q2), s
					if perm[p] in Q2[s]:
						perm.append(perm[p])
						perm[p] = N+1
						fl = True
						break
			elif s == S-1:
				fl = True
				perm = perm + [N+1]
			else:
				raise ValueError('Error -43: Xander sucks. Contact him.')
			
			if fl:
				yield perm
#~ '''
