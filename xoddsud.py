#!/usr/bin/pypy -i

'''
A note to myself, so I stop forgetting:

Set i = d(N^L) + r
Has all tuples with residue d and suffix r

Naively, a Q needs 1 suffix and all residues
and a P would need all suffixes with 1 residue

And this can be mixed for a more optimal approach
'''


HELPSTR='''
xoddsud.py is dedicated to Dr. Sudborough's odd powers.

It uses parity argument to create a good coverage of AGL.
If the --replicate flag is on, it will replicate to create pis

Usage:
	python xoddsud.py Q [OPTIONS]

Where:
	Q is either a prime, a prime power, or a string of the form:
		P^R, where P is prime and R is a prime power

Options:
	-r, --replicate, Create pis
	-p, --peek, will only compute the bound. No match is made. Runs much faster
	-v, --verbose, will print explanatory output
	-w, --write, will write to file without prompting
	-n, --naive, will fully cover N^floor(R/2) cosets plus a freebie per pi,
		no optimization. This can be preferrable because it gives more pis
		of smaller size, but still covers nearly all of AGL. Consider it.

'''.strip()

from xutils.xfield import GaloisField, GaloisElement
from xutils.xflow import match
from xutils.xpe import onlinepe
from xutils import parsePR, gamma, gammainverse
from xgroup import onlinegroupagl
import xtar

from operator import add
import argparse

#convert an integer n into a j length string in base N, padding zeros left
radixTuple = lambda n, j, N: radixTuple(n/N, j-1, N) + (n%N,) if j else tuple()

#return the residue of the sum of the digits of n, mod N
getResidue = lambda n, N=2: (n + getResidue(n/N, N)) % N if n else 0

#Generates sets in Dr. Sudborough's style
def sudSets(f):
	NL = f.P ** f.L
	sets = [list() for _ in xrange(f.P**(f.R/2+1))]
	for e in f.elements:
		sets[e.pre()*NL + e.suf()].append(e)
	return sets

# Calculate the overlap knowing N, a, b, and t (elaborate on this better)
def overlap(N, a, b, t):
	ret = set()
	for G in xrange(a): #G as in P(G)
		for g in xrange(b): #g as in Q(g*t, _)
			ret.add((g*t - G)%N)
	return len(ret)

''' 
Finds the best arrangement of sets that maximizes coverage assuming
the following constraints on arrangement:

If we choose a sets of P and b sets of Q where each of the b sets are
t apart in residue, we must takes another (t-1)a P sets and (t-1)b
Q sets in order to fill the gap we've made. So this routine ignores
the possibility of nesting, which would be complicated anyway, but
possible good for a future version of the program.

O(N^(R+7)) without the trick.
Can be improved to O(N^(R+5)) with a trick
'''
def dpSplit(N, R):
	L = R>>1
	F = N**L
	NF = N*F
	FF = F*F
	n = N**R
	V = [[0]*(NF+1) for _ in xrange(NF+1)]
	S = [[list() for _ in xrange(NF+1)] for _ in xrange(NF+1)]
	
	for p in xrange(1, NF+1): #O(N ^ (R/2+1))
		for q in xrange(1, NF+1): #O(N ^ (R/2+1))
			for a in xrange(1, min(N, p)+1): #O(N)
				for b in xrange(1, min(N, q)+1): #O(N)
					for t in xrange(1, min(N, p/a, q/b)+1): #O(N)
						pot = t*FF*overlap(N, a, b, t) + V[p-t*a][q-t*b] #O(ab), i.e. O(N^2)
						if pot > V[p][q]:
							V[p][q] = pot
							S[p][q] = (a, b, t)

	#retracing
	ret = []
	p, q = NF, NF
	while p>0 and q>0:
		ret.append(S[p][q])
		a, b, t = S[p][q]
		p -= a*t
		q -= b*t
	return V[NF][NF], ret

#Knowing N, R, and a split S, divide the zach sets accordingly
def mishyQ(N, R, S, sets):
	Q = []
	q = 0
	L = R>>1
	F = N**L
	j = 0
	for a, b, t in S:
		qt = [list() for _ in xrange(t)]
		for k in xrange(b*t):
			q, r = divmod(j, N)
			qt[k%t].extend(map(int, sets[r*F + q]))
			j += 1
		Q.extend(qt)

	return Q

#Knowing N, R, and a split S, create the A set accordingly
def mishyA(N, R, S):
	L = R>>1
	F = N**L
	k = 0
	ret = set()
	for a, b, t in S:
		for x in xrange(t):
			for g in xrange(a):
				for h in xrange(F):
					ret.add((k, g, h))
			k += 1
	return ret

# (k, g, h) has an edge to any number q such that if c = gamma(q, k, N^R),
# we have pre(c) = g and suf(c) = h
def E(k, g, h, N, R, sets):
	ret = set()
	L = R>>1
	n = N**R
	for x in sets[h + g*N**L]:
		ret.add(gammainverse(int(x), k, n))
	return ret

# Maximize M(N^R+1, N^R)
def theorem(N, R, prim=None, verbose=False):
	L = R>>1
	F = N**L

	if verbose:
		print 'Computing a parititon for %d^%d = %d' % (N, R, N**R)
		print 'Creating a Galois field...'

	f = GaloisField(N, R, prim)

	if verbose:
		print '\t*', 'Field made:', f.prim
		print 'Creating Sudborough style sets...'

	sets = sudSets(f)

	if verbose:
		print '\t*', 'Sets created!'
		print 'Computing optimal split...'

	V, S = dpSplit(N, R) #number of P to split

	if verbose:
		print '\t*', 'Split calculated:', S, V
		print 'Arranging Q sets...'

	MAR = sum(s[2] for s in S)
	P = [set() for _ in xrange(MAR)]
	Q = mishyQ(N, R, S, sets)

	if verbose:
		print '\t*', 'Q sets arranged!', map(len, Q)
		print 'Arranging P sets...'

	A = mishyA(N, R, S)
	edges = match(A, range(pow(N, R)), lambda u: E(u[0], u[1], u[2], N, R, sets))
	for e in edges:
		P[e[1][0]].add(e[0])

	return P, Q, f, V

# (k, i) has an edge to all numbers in gammainverse( set(i), k, N )  
def naiveE(k, i, N, sets):
	ret = set()
	for x in sets[i]:
		ret.add(gammainverse(int(x), k, N))
	return ret

# Distribute zach's Q sets to align with the split P sets
def naiveQ(N, L, sets):
	#here be dragons
	F = N**L
	Q = []
	for r in xrange(F):
		pot = []
		for d in xrange(N):
			pot.extend(map(int, sets[F*d + r]))
		Q.append(pot)
	return Q

# Create the set A for bipartite matching, splitting the first N*S sets
def naiveA(N, L):
	i = 0
	ret = set()
	for k in xrange(N**L):
		for i in xrange(pow(N, L)):
			ret.add((k, i))
	return ret

def naiveTheorem(N, R, prim=None, verbose=False):
	if verbose:
		print 'Creating a Galois field...'

	f = GaloisField(N, R, prim)
	L = R>>1
	F = N ** L
	
	if verbose:
		print '\t*', 'Field made:', f.prim
		print 'Creating Sudborough style sets...'

	sets = sudSets(f)

	if verbose:
		print '\t*', 'Sets created!'
		print 'Arranging Q sets...'

	P = [set() for _ in xrange(F)]
	Q = naiveQ(N, L, sets)

	if verbose:
		print '\t*', 'Q sets arranged!', map(len, Q)
		print 'Arranging P sets...'

	A = naiveA(N, L)
	edges = match(A, range(NR), lambda u: naiveE(u[0], u[1], f.Q, sets))
	for e in edges:
		P[e[1][0]].add(e[0])

	if verbose:
		print '\t*', 'P sets arranged!', map(len, P)
	return P, Q, f, N**R * (len(P))

def replicate(scriptP, k, N):
	ret = []
	for P in scriptP:
		ret.append([gammainverse(p, k, N) for p in P])
	return ret

def makePis(N, R, verbose=False, naive=False, very_verbose=False):
	NR = N**R
	if verbose:
		print 'Computing a parititon for %d^%d = %d' % (N, R, NR)

	#level 1
	if naive:
		if verbose:
			print u'Exercising na\u00efvit\u00e9...'
		piP, piQ, f, V = naiveTheorem(N, R, verbose=very_verbose)
	else:
		if verbose:
			print "Running optsud..."
		piP, piQ, f, V = theorem(N, R, verbose=very_verbose)

	K = len(piP)
	S = NR / (K+1)

	if verbose:
		print 'Replicating...'

	#replication
	Plv1s = []
	Qlv1s = []
	for s in xrange(S):
		Plv1s.append(replicate(piP, s*(K+1), NR))
		Qlv1s.append(piQ)

	if verbose:
		print 'Generated %d pis of size %d, totalling %.2f%% of AGL' % (S, V+NR, float(NR*(NR-1)) / ((V+NR)*S))

	return Plv1s, Qlv1s, f, (V+NR)*S

from sys import argv
if __name__ == '__main__':
	if len(argv) < 2:
		print HELPSTR
		exit(1)
	
	N, R = parsePR(argv[1])
	NR = N**R
	
	if R%2==0 or R<3:
		print '%d is not an odd power of a prime' % NR
		exit(1)
	
	VERBOSE, PEEK, WRITE, NAIVE, REPLICATE = [False]*5
	for e in argv[2:]:
		if e.lower() in ('-v', '--verbose'):
			VERBOSE = True
		elif e.lower() in ('-p', '--peek'):
			PEEK = True
		elif e in ('-w', '--write'):
			WRITE = True
		elif e in ('-n', '--naive'):
			NAIVE = True
		elif e in ('-r', '--replicate'):
			REPLICATE = True

	if REPLICATE:
		if PEEK:
			if NAIVE:
				K = N ** (R/2)
				S = (NR-1) / (K+1)
				V = NR*K
			else:
				V, split = dpSplit(N, R)
				K = sum(s[2] for s in split)
				S = (NR-1) / (K+1)
				if VERBOSE:
					print split

			print 'Can create %d pis, each with %d cosets (probably)' % (S, K+1)
			print 'With each pi having %d permutations, that is %d permutations total' % (V+NR, S*(V+NR))
			print 'This is %.2f%% of AGL'% (S*(V+NR) / float(NR*(NR-1)))
		else:
			Plv1s, Qlv1s, f, cov = makePis(N, R, verbose=True, naive=NAIVE, very_verbose=VERBOSE)

			print 'Used primitive polynomial:', f.prim
			#~ print 'Created %d pis, covering %d permutations' % (len(Plv1s), cov)
			#~ print 'This is %.2f%% of AGL'% (cov / float(NR*(NR-1)))

			print 'Created %d pis, each with %d cosets' % (len(Plv1s), len(Plv1s[0]))
			print 'That is %d permutations total' % (cov)
			print 'This is %.2f%% of AGL'% (cov / float(NR*(NR-1)))


			if WRITE or raw_input('Write to file? (Y/n): ').lower() != 'n':
				filename = 'Pis_%d_%d_%d_%s.xtar' % (NR+1, len(Plv1s), cov, 'naive' if NAIVE else 'dp')
				
				xtar.compress(filename,
								   filetype='pi',
								   pa=xtar.compresspa(type='agl', field=f),
								   P1=map(list, Plv1s),
								   Q1=map(list, Qlv1s))
				
				print 'Success!'
	else:
		if PEEK:
			if NAIVE:
				V = NR * (N ** (R/2))
			else:
				V, S = dpSplit(N, R)
				if VERBOSE:
					print S

			print 'M(%d, %d) >= %d (probably)' % (NR+1, NR, V+NR)
		else:
			if NAIVE:
				P, Q, f, cov = naiveTheorem(N, R, verbose=VERBOSE)
				#~ cov = N*(N-1) * (N ** (R/2) + 1)
			else:
				P, Q, f, cov = theorem(N, R, verbose=VERBOSE)
			cov += NR

			print 'Used primitive polynomial:', f.prim
			print 'Computed M(%d, %d) >= %d.' % (NR+1, NR, cov)
			if WRITE or raw_input('Write to file? (Y/n): ').lower() != 'n':
				filename = 'M_%d_%d_%d.xtar' % (NR+1, NR, cov)
				
				xtar.compress(filename,
								   filetype='pe',
								   pa=xtar.compresspa(type='agl', field=f),
								   P=map(list, P),
								   Q=map(list, Q))
				
				print 'Success!'
