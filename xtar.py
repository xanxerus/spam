#!/usr/bin/pypy

import xutils.xreverse as xreverse
from xutils.xfield import GaloisField
from xutils import pp, join_pa, parse_pa
from xutils.xverify import naive_hd

if __name__ == '__main__':
	from xgroup import onlinemodagl, onlinegroupagl, onlinepgl, onlinecomposecosets

from sys import argv, stdout
import json

HELPSTR = '''

xcompress handles extracting compressed permutation arrays into readable
formats. The ability to compress a permutation array is in the works.

Usage:
	python xcompress.py INFILE <OPTIONS>

Where:
	INFILE is the name of the file to operate on
	OUTFILE is the name out the file to output to

Options:
	These options are disjoint! Default is quantify.

	-t, --list
		Extract to stdout

	-x, --extract [OUTFILE]
		Extract from INFILE to OUTFILE
		If not given, OUTFILE will be INFILE with a .txt extension
	
	-c, --compress [OUTFILE]
		Compress from INFILE to OUTFILE
		If not given, OUTFILE will be INFILE with a .xtar extension
	
	-q, --quantify
		Give the number of permutations in the file without creating other output

	-v, --verify
		Extract to RAM and report the least pairwise hd

'''.strip()

#~ '''
	#~ -c, --create [OUTFILE]
		#~ Compress from INFILE to OUTFILE
		#~ If not given, OUTFILE will be INFILE with a .xtar extension
#~ '''

'''
Extract will expand a compressed xtar file into a permutation array
and send it somewhere based on the value of the mode parameter.

If mode is 'stream', then the permutation array is written to "out"
	which is another parameter. By default "out" is stdout. 
	If "out" is a string, a file with that filename will be created
	and the permutation array will be written there.

If mode is 'quantify', then this function returns the number of
	permutations in the permutation array.

If mode is 'ram', then this function returns the permutation array

'''
def extract(out=stdout, mode='stream', **kwargs):
	from xutils.xpe import onlinepe, onlinepi, onlinesequential
	from xcontract import onlinecontractedpgl
	from xgroup import onlinecomposecosets

	if mode == 'stream' and isinstance(out, str):
		with open(out, 'w+') as outfile:
			extract(outfile, **kwargs)
		return

	if 'field' in kwargs:
		f = kwargs['field']
		n, r, prim = f.N, f.R, f.prim.coef
	else:
		prim = kwargs.get('prim', None)
		n = kwargs.get('n', None)
		r = kwargs.get('r', 1)
		if n and r and prim:
			f = GaloisField(n, r, prim)
	
	onlinefun = None
	if kwargs['filetype'] == 'pa':
		onlinefun = onlineextractpa(kwargs['pa'])
	elif kwargs['filetype'] == 'contraction':
		dep = kwargs['dep']
		onlinefun = onlinecontractedpgl(f, dep, markers=True)
	elif kwargs['filetype'] == 'pe':
		pa = kwargs['pa']
		P, Q = kwargs['P'], kwargs['Q']
		onlinefun = onlinepe(onlineextractpa(pa), P, Q)
	elif kwargs['filetype'] == 'pi':
		pa = kwargs['pa']
		P1, Q1 = kwargs['P1'], kwargs['Q1']
		onlinefun = onlinepi(onlineextractpa(pa), P1, Q1)
	elif kwargs['filetype'] == 'sequential':
		pa = kwargs['pa']
		P1, Q1 = kwargs['P1'], kwargs['Q1']
		P2, Q2 = kwargs['P2'], kwargs['Q2']
		onlinefun = onlinesequential(onlineextractpa(pa), P1, Q1, P2, Q2)
	elif kwargs['filetype'] == 'cosets':
		pa = kwargs['pa']
		onlinefun = onlinecomposecosets(onlineextractpa(pa), kwargs['reps'])
	else:
		raise ValueError('Unknown filetype: %s cannot be quantified.' % kwargs['filetype'])

	if mode == 'stream': #no stream means quantify
		for perm in onlinefun:
			if perm:
				out.write(pp(perm) + '\n')
			else:
				out.write('\n')
	elif mode == 'generator':
		return onlinefun
	elif mode == 'quantify':
		ret = 0
		for perm in onlinefun:
			if perm:
				ret += 1
		return ret
	elif mode == 'ram':
		pa = []
		coset = []
		for perm in onlinefun:
			if perm:
				coset.append(perm)
			else:
				pa.append(coset)
				coset = []
		return pa

'''
Compress has only one positional argument, called out.

If out is a string, then it will create a file with that name and use that file.
Otherwise, it will assume out is some kind of stream and write to it.

Keyword arguments depend on the filetype. See the documentation on compression.
'''
def compress(out=stdout, **kwargs):
	if isinstance(out, str):
		with open(out, 'w+') as outfile:
			compress(outfile, **kwargs)
		return
	
	if 'field' in kwargs:
		f = kwargs['field']
		kwargs['prim'], kwargs['n'], kwargs['r'] = f.prim.coef, f.P, f.R
		del kwargs['field']

	json.dump(kwargs, out)

def compresspa(**kwargs):
	if 'field' in kwargs:
		f = kwargs['field']
		kwargs['prim'], kwargs['n'], kwargs['r'] = f.prim.coef, f.P, f.R
		del kwargs['field']

	return kwargs

def onlineextractpa(pa):
	from xgroup import onlinemodagl, onlinegroupagl, onlinepgl
	if 'field' in pa:
		f = pa['field']
		n, r, prim = f.N, f.R, f.prim.coef
	else:
		prim = pa.get('prim', None)
		n = pa.get('n', None)
		r = pa.get('r', 1)
		if n and r and prim:
			f = GaloisField(n, r, prim)

	if 'k' in pa:
		k = pa['k']
	else:
		k = n**r - 1
	
	if pa['type'] == 'modagl':
		for e in onlinemodagl(n, k, markers=True):
			yield e
	elif pa['type'] == 'agl':
		for e in onlinegroupagl(f, k, markers=True):
			yield e
	elif pa['type'] == 'pgl':
		for e in onlinepgl(f):
			yield e
	elif pa['type'] == 'misc':
		with open(pa['filename'], 'r') as fin:
			for line in fin:
				if line.strip():
					yield list(map(int, line.strip().split()))
				else:
					yield None

def switchextension(filename, extension):
	while extension[0] == '.':
		extension = extension[1:]
	
	if '.' in filename:
		return '.'.join(filename.split('.')[:-1]) + '.' + extension
	else:
		return filename + '.' + extension

def reconstruct_prompts(cosets):
	print 'Attempting to reconstruct cosets from group AGL'
	f = None
	for perm in cosets[0]:
		if perm[0] != 0:
			f = xreverse.reconstructField(perm)
			print 'Reconstructed field with prim: %s' % f.prim
			break
	
	if f is None:
		print 'Cosets were not AGL.'
		return None
	
	return f

PA_TYPE_PROMPT = '''
'Select the type of permutation array we are compressing:

1. Plain P&E on Group AGL 
2. Sequential P&E on Group AGL 
3. Plain P&E on Modular Arithmetic AGL 
4. Sequential P&E on Modular Arithmetic AGL 
5. Plain P&E on a miscellaneous PA 
6. Sequential P&E on a miscellaneous PA 

'''

def reverse_prompts(block):
	print 'Reverse engineering walkthrough!'
	
	while True:
		try:
			PA_TYPE = int(raw_input(PA_TYPE_PROMPT))
			if 1 <= PA_TYPE <= 6:
				break
		except:
			pass
		
		print 'That is not a valid type'

	if PA_TYPE == 1:
		print 'Simple reversal on Group AGL!'
		print
		f, P, Q, exP, exQ, hasFreebie = xreverse.AGLblockReversal(block)
		return {'filetype':'pe', 'pa':compresspa(**{'type':'agl', 'field':f}), 'P':P, 'Q':Q}

	elif PA_TYPE == 2:
		print 'Sequential reversal on Group AGL!'
		print
		f, P2, Q2, P1, Q1 = xreverse.AGLsequentialReversal(block)
		
		return {'filetype':'sequential', 'pa':compresspa(**{'type':'agl', 'field':f}), 'P1':P1, 'Q1':Q1, 'P2':P2, 'Q2':Q2}

	elif PA_TYPE == 3:
		print 'Simple reversal on Modular Arithmetic AGL!'
		print
		N, P, Q, exP, exQ, hasFreebie = xreverse.modAGLblockReversal(block)
		return {'filetype':'pe', 'pa':compresspa(**{'type':'modagl', 'n':N}), 'P':P, 'Q':Q}

	elif PA_TYPE == 4:
		print 'Sequential reversal on Modular Arithmetic AGL!'
		print
		N, P2, Q2, P1, Q1 = xreverse.modAGLsequentialReversal(block)
		
		return {'filetype':'sequential', 'pa':compresspa(**{'type':'modagl', 'n':N}), 'P1':P1, 'Q1':Q1, 'P2':P2, 'Q2':Q2}

	elif PA_TYPE == 5:
		print 'Simple reversal on miscellaneous PA!'
		print
		cosets, P, Q, exP, exQ, hasFreebie = xreverse.blockReversal(block)
		field = reconstruct_prompts(cosets)
		return {'filetype':'pe', 'pa':compresspa(**{'type':'agl', 'field':field}), 'P':P, 'Q':Q}

	elif PA_TYPE == 6:
		print 'Sequential reversal on miscellaneous PA!'
		print
		cosets, P2, Q2, xP2, xQ2, P1s, Q1s, xP1s, xQ1s, F2, F1s = xreverse.sequentialReversal(block)
		
		if not F2:
			print 'Level 2 did not use a freebie.'
		elif not all(F1s):
			print 'Some cosets in level 1 are missing freebies:', F1s

		field = reconstruct_prompts(cosets)
		return {'filetype':'sequential', 'pa':compresspa(**{'type':'agl', 'field':field}), 'P1':P1s, 'Q1':Q1s, 'P2':P2, 'Q2':Q2}

if __name__ == '__main__':
	if len(argv) < 3 or argv[2] in ('-q', '--quantify'):
		with open(argv[1], 'r') as f:
			print extract(mode='quantify', **json.load(f))
	
	elif argv[2] in ('-t', '--list'):
		with open(argv[1], 'r') as f:
			extract(stdout, **json.load(f))


	elif argv[2] in ('-x', '--extract'):
		with open(argv[1], 'r') as f:
			if len(argv) > 3:
				outfile = argv[3]
			else:
				outfile = switchextension(argv[1], 'txt')

			extract(outfile, **json.load(f))

	elif argv[2] in ('-v', '--verify'):
		print 'Parsing PA file...'
		with open(argv[1], 'r') as f:
			pa = extract(mode='ram', **json.load(f))

		block = join_pa(pa)
		N = len(block[0])

		for i, perm in enumerate(block):
			if len(perm) != N:
				print 'Permutation %d does not have %d symbols' % (i, N)
				break
		else:
			hd = N
			while True:
				pot = naive_hd(block, hd)
				if pot is not None:
					print 'HD of %d is refuted by permutations %d and %d' % (hd, pot[0], pot[1])
					hd -= 1
				else:
					print 'Verified! Hamming distance is:', hd
					break
	elif argv[2] in ('-c', '--compress'):
		if len(argv) > 3:
			outfile = argv[3]
		else:
			outfile = switchextension(argv[1], 'xtar')

		print 'Parsing PA file...'
		block = parse_pa(argv[1], join=True)
		pot = reverse_prompts(block)
		compress(outfile, **pot)
		
	
	else:
		print HELPSTR
